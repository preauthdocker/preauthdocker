package syntel.Provider.ProviderUI.Controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;
import org.apache.http.client.ClientProtocolException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import syntel.Provider.ProviderUI.Connectivity.MySqlCon;
import syntel.Provider.ProviderUI.entity.DrugDetailsEntity;
import syntel.Provider.ProviderUI.model.PADrugDetails;
import syntel.Provider.ProviderUI.model.PALabReports;
import syntel.Provider.ProviderUI.model.PaRequest;
import syntel.Provider.ProviderUI.model.Provider;
import syntel.Provider.ProviderUI.model.ProviderInfo;
import syntel.Provider.ProviderUI.repository.ProviderRepositoryInterface;
import syntel.Provider.ProviderUI.service.DrugService;

@Controller
@RequestMapping("/Provider")
public class ProviderController {

	@Autowired
	ProviderRepositoryInterface repo;

	@Autowired
	DrugService drugServiceObj;

	@GetMapping("/Login")
	public String showLogin() {
		return "LoginPage";
	}

	@GetMapping("/PreAuthHome")
	public String showPreAuthHomePage() {
		return "PreAuthHome";
	}
	
	@GetMapping("/Eligibilty/{memberId}")
	public String showMemberDetails(@PathVariable String memberId, HttpSession session) {
		System.out.println("Member Id = "+memberId);
		TreeSet<String> attributes = new TreeSet<>();
		Enumeration<String> enumeration = session.getAttributeNames();
		while (enumeration.hasMoreElements()) {
			attributes.add(enumeration.nextElement());
		}
		System.out.println(attributes);
		System.out.println(session.getAttribute("jsonData"));
		return "Eligibility";
	}

	@GetMapping("/getDrugDetailsByName/{drugName}")
	public List<DrugDetailsEntity> getDrugDetailsByName(@PathVariable String drugName) {
		List<DrugDetailsEntity> drugDetailsList = drugServiceObj.getDrugDetailsByName(drugName);
		return drugDetailsList;
	}

	@PostMapping("/Login")
	public String validateProvider(Provider provider, Model model, HttpSession session) {
		Provider fetchedProvider = repo.findProviderByNameAndPass(provider.getProvider_Name(),
				provider.getProvider_Pass());

		TreeSet<String> attributes = new TreeSet<>();
		Enumeration<String> enumeration = session.getAttributeNames();
		while (enumeration.hasMoreElements()) {
			attributes.add(enumeration.nextElement());
		}
		System.out.println(attributes);

		System.out.println("FETCHED PROVIDER = " + fetchedProvider);

		if (fetchedProvider != null) {
			session.setAttribute("Provider", fetchedProvider);
			System.out.println("JSON DATA = " + session.getAttribute("jsonData"));

			return "ProviderHome";
		}

		model.addAttribute("error", "Incorrect UserName and Password");
		return "LoginPage";

	}

	@GetMapping("/MemberDetails")
	public String callMemberService(@RequestParam("MemberId") String searchId, HttpSession session, Model model)
			throws ClientProtocolException, IOException {

		// String token = getToken(session);
		// System.out.println("Token aaya kya:-----");
		session.setAttribute("searchId", searchId);
		String inline4 = null;

		// InsurerModel provider=(InsurerModel) session.getAttribute("user");
		// JSONObject obj4=(JSONObject) session.getAttribute("jsonData");

		// System.out.println(obj4.get("request_id"));
		try {
			String APIURL = "http://192.168.99.1:5888/preauth/memberUniData/" + searchId;
			/*
			 * String APIURL =
			 * "https://api.eu.apiconnect.ibmcloud.com/kalyanikatesyntelinccom-dev/sandbox/preauth/getMemberUniData?client_id=78a6e449-54c5-43dd-a296-4c9cdfa71d7f&id="
			 * + searchId;
			 */

			URL url4 = new URL(APIURL);

			try {

				HttpURLConnection conn = (HttpURLConnection) url4.openConnection();

				conn.setRequestMethod("GET");

				// conn.setRequestProperty ("authorization", "Bearer
				// AAIkNzhhNmU0NDktNTRjNS00M2RkLWEyOTYtNGM5Y2RmYTcxZDdmvYnYFZ82yslJHJr196CPbDeG_9owjZ6e02EXe6sZMKThtcxP054zyq3xafsim8TviGni7cK8OfHP3DGfotuhUVbDC-YGyV8_KTAxfYHJqSRhZKGriYMu7WUzjou2ikzN4uNqxmmaBOqAV46vZ0T8jA");
				// conn.setRequestProperty("authorization", "Bearer " + token);
				// System.out.println("Connection Props:=------"+conn.getRequestProperties());
				// conn.setRequestMethod("GET");
				// conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setReadTimeout(15000);
				// conn.setConnectTimeout(15000);
				conn.setDoOutput(false);
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();

				int responsecode = conn.getResponseCode();
				if (responsecode != 200) {
					System.out.println("Exception se upar");
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				} else {
					// System.out.println("Exception ke baad else me");
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					// System.out.println("OPen STrem ke baad");
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline4 = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline4);
					/*
					 * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

		}

		// JSONParser parse= new JSONParser();

		JSONParser jsonParser4 = new JSONParser();
		try {

			// You need to fix this part
			JSONArray jsonArray = (JSONArray) jsonParser4.parse(inline4);
			// System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY"+obj);
			session.setAttribute("jsonData", jsonArray);
			// model.addAttribute("jsonArray", jsonArray);

			Iterator<?> i = jsonArray.iterator();

			if (i.hasNext()) {
				JSONObject obj5 = (JSONObject) i.next();
				session.setAttribute("DrugDetails", obj5);
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + obj5);
				// int tweetID = (int) obj.get("id");
				// String lang = (String) obj.get("lang");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "ProviderHome";
	}
//	

	@GetMapping("/GoHome")
	public String goToHome(HttpSession session) {
		int id = 101;
		Object objSearchId = session.getAttribute("searchIdForHome");
		if (objSearchId != null) {
			int searchId = (int) objSearchId;
			String mem_id = String.valueOf(searchId);
			if (mem_id != null)
				return "redirect:/Provider/MemberDetails?MemberId=" + searchId;
		} else {
			// session.removeAttribute();
			return "ProviderHome";
		}
		return "redirect:/Provider/MemberDetails/";
	}

	@GetMapping("/ViewPaRequest")
	public String viewPaRequest(HttpSession session, Model model) {
		String inline = null;
		Provider provider = (Provider) session.getAttribute("Provider");

		// System.out.println(provider);

		try {

			URL url = new URL("http://192.168.99.1:7001/preauth/allProviderRequests/" + provider.getProvider_ID());
			System.out.println("Provider id" + provider.getProvider_ID());
			try {

				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline = sb.toString();
					System.out.println("112223355444444" + inline);
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		JSONParser jsonParser = new JSONParser();
		try {
			JSONArray jsonArray = (JSONArray) jsonParser.parse(inline);
			System.out.println("JSON ARRAY" + jsonArray);
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jObj = (JSONObject) jsonArray.get(i);
				String dateObj = jObj.get("latest_treatment_date").toString();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				Date date = formatter.parse(dateObj);

				SimpleDateFormat sformatter = new SimpleDateFormat("dd-MMM-yyyy");
				String output = sformatter.format(date);
				jObj.put("latest_treatment_date", output);

			}
			model.addAttribute("jsonArray", jsonArray);
			model.addAttribute("jsonArraySize", jsonArray.size());
			System.out.println(jsonArray.size());
			Iterator<?> i = jsonArray.iterator();

			while (i.hasNext()) {
				JSONObject obj = (JSONObject) i.next();
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ViewPARequest";
	}

	@GetMapping("PostPaRequest/ViewPaRequest")
	public String viewPaRequest2(@RequestParam(required = false) String from, HttpSession session, Model model)
			throws ClassNotFoundException, SQLException {

		if (from != null) {
			int request_id = (int) session.getAttribute("requestId");
			System.out.println(request_id + "from Submitted Page#####");

			PreparedStatement statement = MySqlCon.getConnection()
					.prepareStatement("update pa_request set Current_Status=? where Request_ID=?");

			statement.setString(1, "Request Created");
			statement.setInt(2, request_id);
			statement.execute();
		}
		String inline = null;
		Provider provider = (Provider) session.getAttribute("Provider");

		System.out.println(provider);
		System.out.println("***************" + provider.getProvider_ID());

		try {
			/*
			 * URL url =new
			 * URL("http://192.168.99.1:8001/preauth/allProviderRequests/"+provider.
			 * getProvider_ID());
			 */ // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url = new URL("http://192.168.99.1:7001/preauth/allProviderRequests/" + provider.getProvider_ID());
			System.out.println(
					"Provider IDsjgdfjhasgfdjhsgfjhsgdfjhsdgfjhsdgjfhgsdjfhgsdjfhsgdj" + provider.getProvider_ID());
			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline = sb.toString();
					System.out.println("112223355444444" + inline);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		JSONParser jsonParser = new JSONParser();
		try {
			JSONArray jsonArray = (JSONArray) jsonParser.parse(inline);

			System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY" + jsonArray);
			model.addAttribute("jsonArray", jsonArray);

			Iterator<?> i = jsonArray.iterator();

			while (i.hasNext()) {
				JSONObject obj = (JSONObject) i.next();
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/Provider/ViewPaRequest";
	}

	@GetMapping("/backAction")
	public String doBack() {
		return "MemberForm";
	}

	@GetMapping("/PostPaRequest/{memberId}")
	public String sendPARequestForm(@PathVariable int memberId,
			@RequestParam(name = "requestId", defaultValue = "0") int requestId, HttpSession session) {

		System.out.println("44444$%^^^^" + requestId);
		String inline = null;
		// String token=getToken(session);
		session.setAttribute("searchIdForHome", memberId);
		System.out.println("Member id" + memberId);
		String APIURL = "http://192.168.99.1:5888/preauth/memberData/" + memberId;

		try {

			URL url = new URL(APIURL);

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				// conn.setRequestProperty ("authorization", "Bearer "+token);
				conn.setRequestMethod("GET");
				conn.setRequestProperty("Content-Type", "application/json");

				// conn.setRequestProperty("Content-Type", "application/json");
				conn.connect();

				int responsecode = conn.getResponseCode();

				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

					StringBuilder sb = new StringBuilder();

					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}

					inline = sb.toString();
					System.out.println("54666665757575757577777777777777777" + inline);

				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		JSONParser jsonParser = new JSONParser();
		JSONObject obj;

		try {

			obj = (JSONObject) jsonParser.parse(inline);
			session.setAttribute("jsonData", obj);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "PAPerReg";
	}

	@PostMapping("/PostPaRequest/drugPaRequest")
	public String sendPARequestForm1(PaRequest paRequest, HttpSession session) throws ClassNotFoundException {
		session.setAttribute("PersonalDetails", paRequest);
		int request_id = 0;

		if (session.getAttribute("requestIdToPass") != null) {
			request_id = (int) session.getAttribute("requestIdToPass");
		}

		System.out.println("Sesssion R!!!~~~~~~~~~~~~~~~~~~~~~+&&&&&&&&" + session.getAttribute("requestIdToPass"));
		System.out.println("Reuqetsid in Parequest!!!~~~~~~~~~~~~~~~~~~~~~+&&&&&&&&" + paRequest.getRequest_id());

		try {
			String fetchedRequestIdInString = null;
			PreparedStatement statement2 = MySqlCon.getConnection()
					.prepareStatement("select * from pa_request where Request_ID=?");
			statement2.setInt(1, request_id);
			ResultSet rs = statement2.executeQuery();

			if (!rs.next()) {

				PreparedStatement statement = MySqlCon.getConnection().prepareStatement(
						"insert into pa_request(Member_ID,First_Name,Last_Name,Sex,Age,DOB,Allergies,Insured,Payer_ID,Policy_ID,Latest_Treatment_Date,Vital_Status,Last_Reporting_Source,Provider_ID,Current_Status,comment) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
						Statement.RETURN_GENERATED_KEYS);
				statement.setInt(1, paRequest.getMember_id());
				System.out.println("Memeber IDDDD" + paRequest.getMember_id());
				statement.setString(2, paRequest.getFirst_name());
				statement.setString(3, paRequest.getLast_name());
				statement.setString(4, paRequest.getSex());
				statement.setInt(5, paRequest.getAge());
				statement.setDate(6, paRequest.getDob());
				statement.setString(7, paRequest.getAllergies());
				statement.setString(8, paRequest.getInsured());
				statement.setInt(9, paRequest.getPayer_id());
				statement.setInt(10, paRequest.getPolicy_id());
				statement.setDate(11, paRequest.getLatest_treatment_date());
				statement.setString(12, paRequest.getVital_status());
				statement.setString(13, paRequest.getLast_reporting_source());
				statement.setInt(14, paRequest.getProvider_id());

				statement.setString(15, "Draft Created");
				statement.setString(16, paRequest.getComment());
				System.out.println("pa REQUEST sTATUS AAAAAAAAAAAAA" + paRequest.getCurrent_status());
				statement.execute();

				ResultSet rs1 = statement.getGeneratedKeys();

				if (rs1.next()) {

					request_id = rs1.getInt(1);

					session.setAttribute("requestId", request_id);

					System.out.println("RequestttttttttttttttIdddddddddddddddddddd" + request_id);
				}

				PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
						"insert into comments_audit(Request_ID,Member_ID,Status,Time_Details) values (?,?,?,CURRENT_TIMESTAMP)",
						Statement.RETURN_GENERATED_KEYS);

				statement1.setInt(1, request_id);
				statement1.setInt(2, paRequest.getMember_id());
				// statement1.setString(3, "To Be AutoApproved");

				statement1.setString(3, "Request Created");
				statement1.execute();
			} else {
				int fetchedRequestId = rs.getInt("Request_ID");
				fetchedRequestIdInString = String.valueOf(fetchedRequestId);

				PreparedStatement statement = MySqlCon.getConnection()
						.prepareStatement("update pa_request set comment=? where Request_ID=?");

				statement.setString(1, paRequest.getComment());
				statement.setInt(2, fetchedRequestId);
				statement.execute();

			}
			/*
			 * statement1.setInt(1, request_id); statement1.setInt(2, 125);
			 * statement1.setString(3,"To Be AutoApproved"); statement1.execute();
			 */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * System.out.println("POSSSSStt In Drug PA Request"+paRequest); try {
		 * 
		 * if(paRequest.getMember_id()==0) {
		 * System.out.println("Drug Wala  If chya aaaattttttttttttttt");
		 * PreparedStatement statement = MySqlCon.getConnection().
		 * prepareStatement("insert into pa_request values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
		 * ); statement.setInt(1, paRequest.getRequest_id()); statement.setInt(2,
		 * paRequest.getMember_id());
		 * System.out.println("Memeber IDDD12121212121D"+paRequest.getMember_id());
		 * statement.setString(3,paRequest.getFirst_name());
		 * statement.setString(4,paRequest.getLast_name());
		 * statement.setString(5,paRequest.getSex()); statement.setInt(6,
		 * paRequest.getAge()); statement.setDate(7,paRequest.getDob());
		 * statement.setString(8,paRequest.getInsured()); statement.setInt(9,
		 * paRequest.getPayer_id()); statement.setInt(10, paRequest.getPolicy_id());
		 * statement.setDate(11,paRequest.getLatest_treatment_date());
		 * statement.setString(12,paRequest.getVital_status());
		 * statement.setString(13,paRequest.getLast_reporting_source());
		 * statement.setInt(14, paRequest.getProvider_id());
		 * statement.setString(15,paRequest.getRequest_status());
		 * System.out.println("444444444444444444444444444444444444444");
		 * statement.execute();
		 * System.out.println("5555555555555555555555555555555555555555555"); } else {
		 * System.out.println("Drug Wala  Else chya aaaattttttttttttttt");
		 * //PreparedStatement statement = MySqlCon.getConnection().
		 * prepareStatement("update pa_request(?,?,?,?,?,?,?,?,?,?,?,?,?,? where request_id= ? )"
		 * ); PreparedStatement statement =
		 * MySqlCon.getConnection().prepareStatement("UPDATE pa_request " +
		 * "SET Request_ID = ? ," + "Member_ID = ? ," + "First_Name = ? ," +
		 * "Last_Name = ? ," + "Sex = ? ," + "Age = ? ," + "DOB = ? ," + "Insured = ? ,"
		 * + "Payer_ID = ? ," + "Policy_ID = ? ," + "Latest_Treatment_Date = ? ," +
		 * "Vital_Status = ? ," + "Last_Reporting_Source = ? ," + "Provider_ID = ? ," +
		 * "Request_Status = ? " + "WHERE Member_ID = ?" ); statement.setInt(1,
		 * paRequest.getRequest_id()); statement.setInt(2, paRequest.getMember_id());
		 * System.out.println("Memeber IDD44455666DD"+paRequest.getMember_id());
		 * statement.setString(3,paRequest.getFirst_name());
		 * statement.setString(4,paRequest.getLast_name());
		 * statement.setString(5,paRequest.getSex()); statement.setInt(6,
		 * paRequest.getAge()); statement.setDate(7,paRequest.getDob());
		 * statement.setString(8,paRequest.getInsured()); statement.setInt(9,
		 * paRequest.getPayer_id()); statement.setInt(10, paRequest.getPolicy_id());
		 * statement.setDate(11,paRequest.getLatest_treatment_date());
		 * statement.setString(12,paRequest.getVital_status());
		 * statement.setString(13,paRequest.getLast_reporting_source());
		 * statement.setInt(14, paRequest.getProvider_id());
		 * statement.setString(15,paRequest.getRequest_status()); statement.setInt(16,
		 * paRequest.getMember_id());
		 * System.out.println("77777777777777777777777777777777777777");
		 * statement.execute();
		 * System.out.println("888888888888888888888888888888888888888888"); }
		 * 
		 * } catch (ClassNotFoundException e) { e.printStackTrace(); } catch
		 * (SQLException e) { e.printStackTrace(); }
		 */

		return "ProviderInfo";
	}

	/**** Provider info ****/

	@RequestMapping(value = "/PostPaRequest/providerForm", method = RequestMethod.POST)
	public String addProvider(ProviderInfo providerinfo, HttpSession session) {

		/*
		 * session.setAttribute("ProviderDetails", providerinfo); try {
		 * 
		 * //*************************************** String
		 * fetchedRequestIdInString=null; PreparedStatement statement2 =
		 * MySqlCon.getConnection()
		 * .prepareStatement("select * from Provider where Request_ID=?");
		 * statement2.setInt(1, providerinfo.getRequest_id()); ResultSet rs =
		 * statement2.executeQuery(); if(rs.next()!=false) { int
		 * fetchedRequestId=rs.getInt("Request_ID");
		 * fetchedRequestIdInString=String.valueOf(fetchedRequestId); }
		 * if(fetchedRequestIdInString==null) { String
		 * requestIdInString=String.valueOf(providerinfo.getRequest_id()); if
		 * (requestIdInString!=null) { System.out.println("providerr If chya aat");
		 * 
		 * PreparedStatement statement = MySqlCon.getConnection()
		 * .prepareStatement("insert into   provider_info values(?,?)");
		 * statement.setInt(1, providerinfo.getProvider_id()); statement.setInt(2,
		 * providerinfo.getRequest_id()); statement.setString(3,
		 * providerinfo.getProvider_name()); statement.setString(4,
		 * providerinfo.getProvider_type()); statement.setString(5,
		 * providerinfo.getSpeciality()); statement.setInt(6,
		 * providerinfo.getPhone_no()); statement.setString(7,
		 * providerinfo.getAddress()); statement.execute(); } } else {
		 * System.out.println("Elseeeeeeeeeeeeeee" + providerinfo.getRequest_id());
		 * PreparedStatement statement = MySqlCon.getConnection().prepareStatement(
		 * "update  Providerinfo set provider_id =?,Request_id=?"); statement.setInt(1,
		 * providerinfo.getProvider_id()); statement.setInt(2,
		 * providerinfo.getRequest_id()); statement.setString(3,
		 * providerinfo.getProvider_name()); statement.setString(4,
		 * providerinfo.getProvider_type()); statement.setString(5,
		 * providerinfo.getSpeciality()); statement.setInt(6,
		 * providerinfo.getPhone_no()); statement.setString(7,
		 * providerinfo.getAddress());
		 * 
		 * statement.execute(); }
		 * 
		 * } catch (ClassNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (SQLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */

		return "DrugFormSubmit";
	}

	@RequestMapping(value = "/PostPaRequest/postDrugForm", method = RequestMethod.POST)
	public String addDrugs(PADrugDetails paDrugDetails, HttpSession session) {
		System.out.println("Drug details = " + paDrugDetails);
		/*
		 * List<PADrugDetails> drugList= new ArrayList<PADrugDetails>(); for(int i=0;
		 * i<drug.getDrugDetails().size();i++) {
		 * drugList.add(drug.getDrugDetails().get(i)); }
		 */

		session.setAttribute("DrugDetails", paDrugDetails);
		try {

			// ***************************************
			String fetchedRequestIdInString = null;
			PreparedStatement statement2 = MySqlCon.getConnection()
					.prepareStatement("select * from pa_drug_details where Request_ID=?");
			statement2.setInt(1, paDrugDetails.getRequest_id());
			ResultSet rs = statement2.executeQuery();
			if (rs.next() != false) {
				int fetchedRequestId = rs.getInt("Request_ID");
				fetchedRequestIdInString = String.valueOf(fetchedRequestId);
			}
			if (fetchedRequestIdInString == null) {
				String requestIdInString = String.valueOf(paDrugDetails.getRequest_id());
				if (requestIdInString != null) {
					System.out.println("drugggg If chya aat");
					// ****************************************

					/*
					 * for (PADrugDetails paDrugDetails : drugList) {
					 */
					PreparedStatement statement = MySqlCon.getConnection()
							.prepareStatement("insert into   pa_drug_details values(?,?,?,?,?,?,?,?,?,?,?)");

					statement.setInt(1, paDrugDetails.getDrug_id());
					statement.setInt(2, paDrugDetails.getRequest_id());
					statement.setString(3, paDrugDetails.getDisease());
					statement.setString(4, paDrugDetails.getDrug_name());
					statement.setString(5, paDrugDetails.getDrug_substitute());
					statement.setString(6, paDrugDetails.getStrength());
					statement.setString(7, paDrugDetails.getDuration());
					statement.setString(8, paDrugDetails.getClinical_info());
					statement.setString(9, paDrugDetails.getAllow_combination());
					statement.setString(10, paDrugDetails.getMax_dosage());
					statement.setString(11, "Drug Comments");
					statement.execute();
				}
			} else {
				System.out.println("Elseeeeeeeeeeeeeee" + paDrugDetails.getRequest_id());
				PreparedStatement statement = MySqlCon.getConnection().prepareStatement(
						"update  pa_drug_details set Drug_ID=?,Disease=?,Drug_Name=?,Drug_Substitute=?,Max_Dosage=?,Allow_Combination=?,Strength=?,Duration=?,Clinical_info= ?, Drug_Comments=? where Request_ID = ?");
				statement.setInt(1, paDrugDetails.getDrug_id());
				statement.setString(2, paDrugDetails.getDisease());
				statement.setString(3, paDrugDetails.getDrug_name());
				statement.setString(4, paDrugDetails.getDrug_substitute());
				statement.setString(5, paDrugDetails.getMax_dosage());
				statement.setString(6, paDrugDetails.getAllow_combination());
				statement.setString(7, paDrugDetails.getStrength());
				statement.setString(8, paDrugDetails.getDuration());
				statement.setString(9, paDrugDetails.getClinical_info());
				statement.setString(10, paDrugDetails.getComment());
				statement.setInt(11, paDrugDetails.getRequest_id());
				statement.execute();
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/* return "LabFinalReports"; */
		return "SummaryPage";
	}

	/*
	 * @PostMapping("/PostPaRequest/submitFinalForm") public String
	 * sendFinalForm(PALabReports labReports, HttpSession session) {
	 * 
	 * session.setAttribute("LabDetails", labReports);
	 * 
	 * try { String fetchedRequestIdInString = null; PreparedStatement statement2 =
	 * MySqlCon.getConnection()
	 * .prepareStatement("select * from pa_lab_reports where Request_ID=?");
	 * statement2.setInt(1, labReports.getRequest_id()); ResultSet rs =
	 * statement2.executeQuery(); if (rs.next() != false) { int fetchedRequestId =
	 * rs.getInt("Request_ID"); fetchedRequestIdInString =
	 * String.valueOf(fetchedRequestId); } // System.out.println(); if
	 * (fetchedRequestIdInString == null) { String requestIdInString =
	 * String.valueOf(labReports.getRequest_id()); if (requestIdInString != null) {
	 * System.out.println("If chya aat"); PreparedStatement statement =
	 * MySqlCon.getConnection()
	 * .prepareStatement("insert into pa_lab_reports values(?,?,?,?,?,?,?,?,?)");
	 * 
	 * statement.setInt(1, labReports.getRequest_id()); statement.setString(2,
	 * labReports.getBlood_pressure()); statement.setString(3,
	 * labReports.getDiastolic()); statement.setString(4, labReports.getSystolic());
	 * statement.setString(5, labReports.getHeamoglobin()); statement.setString(6,
	 * labReports.getWbc()); statement.setString(7, labReports.getRbc()); //
	 * statement.setString(6, "6000"); // statement.setString(7, "4500");
	 * statement.setString(8, labReports.getBlood_sugar_fasting());
	 * statement.setString(9, labReports.getHiv_status()); statement.execute(); } }
	 * else { System.out.println("Elseeeeeeeeeeeeeee" + labReports.getRequest_id());
	 * PreparedStatement statement = MySqlCon.getConnection().prepareStatement(
	 * "update pa_lab_reports set Blood_Pressure=?,Diastolic=?,Systolic=?,Heamoglobin=?,WBC=?,RBC=?,BloodSugar_Fasting=?,HIV_Status=? where Request_ID = ?"
	 * ); statement.setString(1, labReports.getBlood_pressure());
	 * statement.setString(2, labReports.getDiastolic()); statement.setString(3,
	 * labReports.getSystolic()); statement.setString(4,
	 * labReports.getHeamoglobin()); statement.setString(5, labReports.getWbc());
	 * statement.setString(6, labReports.getRbc()); // statement.setString(5,
	 * "6000"); // statement.setString(6, "4500"); statement.setString(7,
	 * labReports.getBlood_sugar_fasting()); statement.setString(8, "-ve");
	 * statement.setInt(9, labReports.getRequest_id()); statement.execute(); } }
	 * catch (ClassNotFoundException e) { e.printStackTrace(); } catch (SQLException
	 * e) { e.printStackTrace(); } finally { try { MySqlCon.getConnection().close();
	 * } catch (ClassNotFoundException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (SQLException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); } }
	 * 
	 * return "SummaryPage"; }
	 */
	@GetMapping("PostPaRequest/editInSummaryPage/{requestId}")
	public String editRejected1(PALabReports labReports, HttpSession session, Model model,
			@PathVariable int requestId) {
		session.setAttribute("requestId", requestId);
		String inline1 = null;
		System.out.println("MUSAAAAAAAAAA" + requestId);
		try {

			/*
			 * * URL url =new URL("http://192.168.99.1:8003/preauth/labPARequest/"+requestId);
			 */
			// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url = new URL("http://192.168.99.1:5003/preauth/labPARequest/" + requestId);

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline1 = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline1);

					/*
					 * * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */

				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		// JSONParser parse= new JSONParser();
		JSONObject obj;
		JSONParser jsonParser = new JSONParser();
		try {

			// You need to fix this part
			obj = (JSONObject) jsonParser.parse(inline1);
			System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY" + obj);
			session.setAttribute("LabDetails", obj);
			// model.addAttribute("jsonArray", jsonArray);

			/*
			 * * Iterator<?> i = jsonArray.iterator();
			 * 
			 * while (i.hasNext()) { JSONObject obj = (JSONObject) i.next();
			 * System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+obj);
			 * // int tweetID = (int) obj.get("id"); //String lang = (String)
			 * obj.get("lang"); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

//************************		
		String inline = null;

		try {

			/*
			 * * URL url =new
			 * URL("http://192.168.99.1:8001/preauth/allPersonalRequests/"+requestId);
			 */
			// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url = new URL("http://192.168.99.1:7001/preauth/allPersonalRequests/" + requestId);
			System.out.println("OBJect IDDDDDDDDDDDDDDDDDDDDDD555555555555" + requestId);

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline);

					/*
					 * * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */

				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		// JSONParser parse= new JSONParser();

		JSONParser jsonParser1 = new JSONParser();
		try {

			// You need to fix this part
			JSONObject obj1 = (JSONObject) jsonParser1.parse(inline);
			// System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY"+obj);
			session.setAttribute("PersonalDetails", obj1);
			System.out.println("sahdgsjhgfjashgfdjsghdfjhsdgfjghsdfjghsj" + obj1);
			// model.addAttribute("jsonArray", jsonArray);

			// Iterator<?> i = jsonArray.iterator();

			/*
			 * * while (i.hasNext()) { JSONObject obj1 = (JSONObject) i.next();
			 * System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+obj1);
			 * // int tweetID = (int) obj.get("id"); //String lang = (String)
			 * obj.get("lang"); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ***********************DrugDetails***********

		String inline4 = null;

		// InsurerModel provider=(InsurerModel) session.getAttribute("user");
		// JSONObject obj4=(JSONObject) session.getAttribute("jsonData");

		// System.out.println(obj4.get("request_id"));
		try {

			/*
			 * * URL url4 =new
			 * URL("http://192.168.99.1:8002/preauth/medicinePARequest/"+requestId);
			 */
			// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url4 = new URL("http://192.168.99.1:7002/preauth/medicinePARequest/" + requestId);

			try {

				HttpURLConnection conn = (HttpURLConnection) url4.openConnection();
				conn.setRequestMethod("GET");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url4.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline4 = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline4);

					/*
					 * * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */

				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		// JSONParser parse= new JSONParser();

		JSONParser jsonParser4 = new JSONParser();
		try {

			// You need to fix this part
			JSONArray jsonArray = (JSONArray) jsonParser4.parse(inline4);
			// System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY"+obj);
			session.setAttribute("jsonArray", jsonArray);
			// model.addAttribute("jsonArray", jsonArray);

			Iterator<?> i = jsonArray.iterator();

			if (i.hasNext()) {
				JSONObject obj5 = (JSONObject) i.next();
				session.setAttribute("DrugDetails", obj5);
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + obj5);
				// int tweetID = (int) obj.get("id");
				// String lang = (String) obj.get("lang");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/Provider/summaryPostPaRequest";
	}

	/*
	 * #############################################################################
	 * ###########################
	 */
	@GetMapping("/editInSummaryPage/{requestId}")
	public String editRejected(PALabReports labReports, HttpSession session, Model model, @PathVariable int requestId) {
		session.setAttribute("requestId", requestId);
		String inline1 = null;
		System.out.println("MUSAAAAAAAAAA" + requestId);
		/*
		 * try {
		 * 
		 * URL url =new URL("http://192.168.99.1:8003/preauth/labPARequest/"+requestId);
		 * // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 * @@@@@@@@@@@@@@@@@@@@@@@@@@ URL url = new
		 * URL("http://192.168.99.1:5003/preauth/labPARequest/" + requestId);
		 * 
		 * try { HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		 * conn.setRequestMethod("GET"); //
		 * conn.setRequestProperty("Authorization","Bearer //
		 * AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z
		 * -_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-
		 * C5SlhsSQaU5vGuJXgh3g0dA"); conn.setRequestProperty("Content-Type",
		 * "application/json"); // conn.setRequestProperty("accept",
		 * "application/json"); // System.out.println(
		 * "Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.
		 * getHeaderFields()); conn.connect(); int responsecode =
		 * conn.getResponseCode(); if (responsecode != 200) throw new
		 * RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode); else {
		 * BufferedReader reader = new BufferedReader(new
		 * InputStreamReader(url.openStream())); StringBuilder sb = new StringBuilder();
		 * String line = null;
		 * 
		 * while ((line = reader.readLine()) != null) { sb.append(line + "\n"); }
		 * inline1 = sb.toString(); // Scanner sc = new Scanner(url.openStream()); //
		 * System.out.println("54666665757575757577777777777777777"+url.openStream().
		 * read()); System.out.println("54666665757575757577777777777777777" + inline1);
		 * 
		 * while(sc.hasNext()) {
		 * 
		 * inline=inline+sc.nextLine(); }
		 * System.out.println("\nJSON data in string format");
		 * System.out.println(inline); sc.close();
		 * 
		 * } } catch (IOException e) {
		 * 
		 * e.printStackTrace(); } } catch (MalformedURLException e) {
		 * 
		 * e.printStackTrace(); }
		 */
		// JSONParser parse= new JSONParser();
		/*
		 * JSONObject obj; JSONParser jsonParser = new JSONParser(); try {
		 * 
		 * // You need to fix this part obj = (JSONObject) jsonParser.parse(inline1);
		 * System.out.println(
		 * "JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOANARRRRRRRRRRRRRRRRRRAAAAYYY" + obj);
		 * session.setAttribute("LabDetails", obj); // model.addAttribute("jsonArray",
		 * jsonArray);
		 * 
		 * 
		 * Iterator<?> i = jsonArray.iterator();
		 * 
		 * while (i.hasNext()) { JSONObject obj = (JSONObject) i.next();
		 * System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+obj);
		 * // int tweetID = (int) obj.get("id"); //String lang = (String)
		 * obj.get("lang"); }
		 * 
		 * } catch (Exception e) { e.printStackTrace(); }
		 */

//************************		

		String inline = null;

		try {
			/*
			 * URL url =new
			 * URL("http://192.168.99.1:8001/preauth/allPersonalRequests/"+requestId);
			 */ // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url = new URL("http://192.168.99.1:7001/preauth/allPersonalRequests/" + requestId);
			System.out.println("OBJect IDDDDDDDDDDDDDDDDDDDDDD555555555555" + requestId);

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline);
					/*
					 * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		// JSONParser parse= new JSONParser();

		JSONParser jsonParser1 = new JSONParser();
		try {

			// You need to fix this part
			JSONObject obj1 = (JSONObject) jsonParser1.parse(inline);
			// System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY"+obj);
			session.setAttribute("PersonalDetails", obj1);
			System.out.println("sahdgsjhgfjashgfdjsghdfjhsdgfjghsdfjghsj" + obj1);
			// model.addAttribute("jsonArray", jsonArray);

			// Iterator<?> i = jsonArray.iterator();

			/*
			 * while (i.hasNext()) { JSONObject obj1 = (JSONObject) i.next();
			 * System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+obj1);
			 * // int tweetID = (int) obj.get("id"); //String lang = (String)
			 * obj.get("lang"); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		// *****************Provider Form*******************//

		// ***********************DrugDetails***********

		String inline4 = null;

		// InsurerModel provider=(InsurerModel) session.getAttribute("user");
		// JSONObject obj4=(JSONObject) session.getAttribute("jsonData");

		// System.out.println(obj4.get("request_id"));
		try {
			/*
			 * URL url4 =new
			 * URL("http://192.168.99.1:8002/preauth/medicinePARequest/"+requestId);
			 */ // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url4 = new URL("http://192.168.99.1:7002/preauth/medicinePARequest/" + requestId);

			try {

				HttpURLConnection conn = (HttpURLConnection) url4.openConnection();
				conn.setRequestMethod("GET");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url4.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline4 = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline4);
					/*
					 * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		// JSONParser parse= new JSONParser();

		JSONParser jsonParser4 = new JSONParser();
		try {

			// You need to fix this part
			JSONArray jsonArray = (JSONArray) jsonParser4.parse(inline4);
			// System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY"+obj);
			session.setAttribute("jsonArray", jsonArray);
			// model.addAttribute("jsonArray", jsonArray);

			Iterator<?> i = jsonArray.iterator();

			if (i.hasNext()) {
				JSONObject obj5 = (JSONObject) i.next();
				session.setAttribute("DrugDetails", obj5);
				System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" + obj5);
				// int tweetID = (int) obj.get("id");
				// String lang = (String) obj.get("lang");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
					"select Comments from comments_audit where Request_ID = ? order by Time_Details desc limit 1");
			statement1.setInt(1, requestId);
			ResultSet rs = statement1.executeQuery();

			String comment = null;
			while (rs.next()) {
				comment = rs.getString("Comments");
			}

			if (comment == null) {
				model.addAttribute("CommentDetails", "No Comment for this Request");
			}

			session.setAttribute("CommentDetails", comment);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "redirect:/Provider/summaryPostPaRequest";
	}
	/*
	 * #############################################################################
	 * ###########################
	 */

	@GetMapping("/logout")

	public String logOut(HttpSession session) {
		session.invalidate();
		return "redirect:Login";
	}

	@GetMapping("/PostPaRequest/logout")
	public String logOut2(HttpSession session) {
		System.out.println("Logout me aaya postpa request ki");
		session.invalidate();
		return "redirect:/Provider/Login";
	}

	@GetMapping("/drugPaRequest/logout")
	public String logOut3(HttpSession session) {
		session.invalidate();
		return "redirect:Login";
	}

	@GetMapping("/postDrugForm/logout")
	public String logOut4(HttpSession session) {
		session.invalidate();
		return "redirect:Login";
	}

	@GetMapping("/summaryPostPaRequest")
	public String summaryPersonal(HttpSession session, PaRequest paRequest) {
		System.out.println(paRequest);
		// int requestId = (int) session.getAttribute("requestId");
		System.out.println(paRequest + "hello");
		/* session.se */
		/*
		 * try { PreparedStatement statement2 = MySqlCon.getConnection().
		 * prepareStatement("select * from pa_request where Request_ID = ?");
		 * statement2.setInt(1, paRequest.getRequest_id()); ResultSet rs=
		 * statement2.executeQuery();
		 * 
		 * while(rs.next()) {
		 * 
		 * System.out.println("              jkwksjklwksleks"+ rs.getInt("Request_ID"));
		 * } } catch (SQLException | ClassNotFoundException e1) { // TODO Auto-generated
		 * catch block e1.printStackTrace(); }
		 */

		return "SummaryPage3";
	}

	@GetMapping("PostPaRequest/summaryPostPaRequest")
	public String summaryPersonal3(HttpSession session, PaRequest paRequest) {
		System.out.println(paRequest);
		int requestId = (int) session.getAttribute("requestId");
		System.out.println(requestId);
		System.out.println("hello");
		/* session.se */
		/*
		 * try { PreparedStatement statement2 = MySqlCon.getConnection().
		 * prepareStatement("select * from pa_request where Request_ID = ?");
		 * statement2.setInt(1, paRequest.getRequest_id()); ResultSet rs=
		 * statement2.executeQuery();
		 * 
		 * while(rs.next()) {
		 * 
		 * System.out.println("              jkwksjklwksleks"+ rs.getInt("Request_ID"));
		 * } } catch (SQLException | ClassNotFoundException e1) { // TODO Auto-generated
		 * catch block e1.printStackTrace(); }
		 */

		// return "ViewPARequest";
		return "SummaryPage3";
	}

	@PostMapping("/PostPaRequest/summarydrugPaRequest")
	public String summaryDrugDetails(HttpSession session, PADrugDetails paDrug, Model model) {
		System.out.println(paDrug);
		session.setAttribute("DrugDetails", paDrug);

		try {

			PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
					"update pa_drug_details set Drug_ID = ?,Disease = ?,Drug_Name = ?,Drug_Substitute = ?,Max_Dosage = ?, Allow_Combination = ?, Strength= ?,Duration= ?,Clinical_info= ?, Drug_Comments=? where pa_drug_details.Request_ID = ? ");

			statement1.setInt(1, paDrug.getDrug_id());
			statement1.setString(2, paDrug.getDisease());
			statement1.setString(3, paDrug.getDrug_name());
			statement1.setString(4, paDrug.getDrug_substitute());
			statement1.setString(5, paDrug.getMax_dosage());
			statement1.setString(6, paDrug.getAllow_combination());
			statement1.setString(7, paDrug.getStrength());
			statement1.setString(8, paDrug.getDuration());
			statement1.setString(9, paDrug.getClinical_info());
			statement1.setString(10, paDrug.getComment());
			statement1.setInt(11, paDrug.getRequest_id());

			/*
			 * statement1.setString(1, "Referred To CM"); statement1.setInt(2, 10011);
			 */

			statement1.execute();
			/* bnb */

			PreparedStatement statement3 = MySqlCon.getConnection()
					.prepareStatement("select Member_ID  from pa_request where Request_ID = ?");
			statement3.setInt(1, paDrug.getRequest_id());
			ResultSet rs = statement3.executeQuery();
			int mem_id = 0;
			while (rs.next()) {

				mem_id = rs.getInt("Member_ID");

			}

			PreparedStatement statement2 = MySqlCon.getConnection().prepareStatement(
					"insert into comments_audit(Request_ID,Member_ID,Status,Comments,Time_Details) values (?,?,?,?,CURRENT_TIMESTAMP)",
					Statement.RETURN_GENERATED_KEYS);
			statement2.setInt(1, paDrug.getRequest_id());
			statement2.setInt(2, mem_id);
			// statement2.setString(3, "To Be AutoApproved");
			statement2.setString(3, "Request Created");
			// statement2.setString(3, "Request to be Auto Approve");
			statement2.setString(4, paDrug.getComment());

			statement2.execute();
			/* bnb */

//			if(result==true)
//			{
			model.addAttribute("updatedMessage", "Your Details Are Updated Successfully");
			/* } */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "SummaryPage3";
	}

	@GetMapping("PostPaRequest/deleteWithBack/{requestId}")
	public String doBackAndDelete(@PathVariable int requestId, HttpSession session) {
		PaRequest paRequest = new PaRequest();
		System.out.println("#$%^&()*&%^*&%&*^%&*%^&^%^%&**)&(*" + requestId);
		// int id=Integer.parseInt(requestId);
		try {

			ResultSet rs = null;
			PreparedStatement statement1 = MySqlCon.getConnection()
					.prepareStatement("select * from pa_request where Request_ID = ?");
			statement1.setInt(1, requestId);
			rs = statement1.executeQuery();

			if (rs.next() == true) {
				paRequest.setRequest_id(rs.getInt("Request_ID"));
				paRequest.setMember_id(rs.getInt("Member_ID"));
				paRequest.setFirst_name(rs.getString("First_Name"));
				paRequest.setLast_name(rs.getString("Last_Name"));
				paRequest.setSex(rs.getString("Sex"));
				paRequest.setAge(rs.getInt("Age"));
				paRequest.setDob(rs.getDate("DOB"));
				paRequest.setInsured(rs.getString("Insured"));
				paRequest.setPayer_id(rs.getInt("Payer_ID"));
				paRequest.setPolicy_id(rs.getInt("Policy_ID"));
				paRequest.setLatest_treatment_date(rs.getDate("Latest_Treatment_Date"));
				paRequest.setVital_status(rs.getString("Vital_Status"));
				paRequest.setLast_reporting_source(rs.getString("Last_Reporting_Source"));
				paRequest.setProvider_id(rs.getInt("Provider_ID"));
				paRequest.setCurrent_status(rs.getString("Current_Status"));
			}
			// return
			// "redirect:/Provider/PostPaRequest/"+paRequest.getMember_id()+"?requestId="+paRequest.getRequest_id();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return "redirect:/Provider/PostPaRequest/drugPaRequest";
		session.setAttribute("requestIdToPass", paRequest.getRequest_id());
		return "redirect:/Provider/PostPaRequest/" + paRequest.getMember_id() + "?requestId="
				+ paRequest.getRequest_id();
	}

	@PostMapping("summarydrugPaRequest")
	public String summaryDrugDetails2(HttpSession session, PADrugDetails paDrug, Model model) {
		System.out.println(paDrug);
		session.setAttribute("DrugDetails", paDrug);
		System.out.println("Chal kon sa rha hai");
		try {
			System.out.println("Chal kon sa rha hai  2222222");
			PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
					"update pa_drug_details set Drug_ID = ?,Disease = ?,Drug_Name = ?,Drug_Substitute = ?,Max_Dosage = ?,Allow_Combination = ?,Strength= ?,Duration= ?,Clinical_info= ?,  Drug_Comments=? where pa_drug_details.Request_ID = ? ");

			statement1.setInt(1, paDrug.getDrug_id());
			statement1.setString(2, paDrug.getDisease());
			statement1.setString(3, paDrug.getDrug_name());
			statement1.setString(4, paDrug.getDrug_substitute());
			statement1.setString(5, paDrug.getMax_dosage());
			statement1.setString(6, paDrug.getAllow_combination());
			statement1.setString(7, paDrug.getStrength());
			statement1.setString(8, paDrug.getDuration());
			statement1.setString(9, paDrug.getClinical_info());
			statement1.setString(10, paDrug.getComment());
			statement1.setInt(11, paDrug.getRequest_id());

			System.out.println("True ya false" + paDrug.getComment());

			/*
			 * statement1.setString(1, "Referred To CM"); statement1.setInt(2, 10011);
			 */

			statement1.execute();

			PreparedStatement statement3 = MySqlCon.getConnection()
					.prepareStatement("select Member_ID  from pa_request where Request_ID = ?");
			statement3.setInt(1, paDrug.getRequest_id());
			ResultSet rs = statement3.executeQuery();
			int mem_id = 0;
			while (rs.next()) {

				mem_id = rs.getInt("Member_ID");

			}

			PreparedStatement statement = MySqlCon.getConnection().prepareStatement(
					"insert into comments_audit(Request_ID,Member_ID,Status,Comments,Time_Details) values (?,?,?,?,CURRENT_TIMESTAMP)",
					Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, paDrug.getRequest_id());
			statement.setInt(2, mem_id);
			// statement.setString(3, "Request to be Auto Approve");
			// statement.setString(3, "To Be AutoApproved");
			statement.setString(3, "Request Created");
			// statement.setString(4, paDrug.getComment());

			statement.execute();

			/*
			 * statement.setInt(1,10011); statement.setInt(2, 125); statement.setString(3,
			 * "Referred To CM"); statement.setString(4, "Everything is fine");
			 * statement.execute();
			 */
			PreparedStatement statement2 = MySqlCon.getConnection().prepareStatement(
					"update pa_request set pa_request.Current_Status = ?  where pa_request.Request_ID = ? ");

			// statement2.setString(1, "To Be AutoApproved");
			statement2.setString(1, "Request Created");
			// statement2.setString(1, "Request To Be Auto Approve");
			statement2.setInt(2, paDrug.getRequest_id());
			statement2.execute();

			System.out.println("REsult HAi yeh");

//			if(result==true)
//			{
			model.addAttribute("updatedMessage", "Your Details Are Updated Successfully");

			/*
			 * try {
			 * 
			 * PreparedStatement statement1 = MySqlCon.getConnection().
			 * prepareStatement("update pa_drug_details set Drug_ID = ?,Disease = ?,Drug_Name = ?,Drug_Substitute = ?,Max_Dosage = ?,Allow_Combination = ?  where pa_drug_details.Request_ID = ? "
			 * );
			 * 
			 * statement1.setInt(1, paDrug.getDrug_id()); statement1.setString(2,
			 * paDrug.getDisease()); statement1.setString(3, paDrug.getDrug_name());
			 * statement1.setString(4, paDrug.getDrug_substitute()); statement1.setString(5,
			 * paDrug.getMax_dosage()); statement1.setString(6,
			 * paDrug.getAllow_combination()); statement1.setInt(7, paDrug.getRequest_id());
			 * 
			 * 
			 * statement1.execute();
			 * 
			 * 
			 * System.out.println("REsult HAi yeh");
			 * 
			 * // if(result==true) // {
			 * model.addAttribute("updatedMessage","Your Details Are Updated Successfully"
			 * ); }
			 */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "SummaryPage3";
	}

	/*
	 * @PostMapping("PostPaRequest/summaryLabForm") public String
	 * summaryLabDetails(HttpSession session, PALabReports paLab, Model model) {
	 * session.setAttribute("LabDetails", paLab); System.out.println(paLab); try {
	 * 
	 * PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
	 * "update  pa_lab_reports set Blood_Pressure = ?,Diastolic = ?,Systolic = ?,Heamoglobin = ?,WBC = ?,RBC = ?, BloodSugar_Fasting = ?,HIV_Status = ? where pa_lab_reports.Request_ID = ? "
	 * );
	 * 
	 * statement1.setString(1, paLab.getBlood_pressure()); statement1.setString(2,
	 * paLab.getDiastolic()); statement1.setString(3, paLab.getSystolic());
	 * statement1.setString(4, paLab.getHeamoglobin()); statement1.setString(5,
	 * paLab.getWbc()); statement1.setString(6, paLab.getRbc()); //
	 * statement1.setString(5, "6000"); // statement1.setString(6, "4500");
	 * statement1.setString(7, paLab.getBlood_sugar_fasting());
	 * statement1.setString(8, paLab.getHiv_status()); statement1.setInt(9,
	 * paLab.getRequest_id());
	 * 
	 * statement1.setString(1, "Referred To CM"); statement1.setInt(2, 10011);
	 * 
	 * 
	 * statement1.execute(); System.out.println("REsult HAi yeh");
	 * 
	 * // if(result==true) // { model.addAttribute("updatedMessage",
	 * "Your Details Are Updated Successfully"); }
	 * 
	 * } catch (ClassNotFoundException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (SQLException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * 
	 * return "SummaryPage3"; }
	 */

	/* ...summaryComment.. */
	@PostMapping("summaryComment")
	public String summaryComment(HttpSession session, PADrugDetails paDrug, Model model) {
		System.out.println("mainnnnnnnnnnn" + paDrug);
		session.setAttribute("DrugDetails", paDrug);
		System.out.println("before try");

		try {
			System.out.println("in try");
			PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
					"update pa_drug_details set Drug_Comments=? where pa_drug_details.Request_ID = ? ");

			statement1.setString(1, paDrug.getComment());
			System.out.println("PA Drug Details" + paDrug.getComment());
			statement1.setInt(2, paDrug.getRequest_id());
			System.out.println("PA Drug Details" + paDrug.getRequest_id());

			statement1.execute();

			System.out.println("REsult HAi yeh");

			/*
			 * if(result==true) {
			 * model.addAttribute("updatedMessage","Your Details Are Updated Successfully"
			 * ); }
			 */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "SummaryPage3";
	}/* ...... */

	/*
	 * @PostMapping("summaryLabForm") public String summaryLabDetails3(HttpSession
	 * session, PALabReports paLab, Model model) {
	 * session.setAttribute("LabDetails", paLab); System.out.println(paLab); try {
	 * 
	 * PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
	 * "update  pa_lab_reports set Blood_Pressure = ?,Diastolic = ?,Systolic = ?,Heamoglobin = ?,WBC = ?,RBC = ?, BloodSugar_Fasting = ?,HIV_Status = ? where pa_lab_reports.Request_ID = ? "
	 * );
	 * 
	 * statement1.setString(1, paLab.getBlood_pressure()); statement1.setString(2,
	 * paLab.getDiastolic()); statement1.setString(3, paLab.getSystolic());
	 * statement1.setString(4, paLab.getHeamoglobin()); statement1.setString(5,
	 * paLab.getWbc()); statement1.setString(6, paLab.getRbc()); //
	 * statement1.setString(5, "200000"); // statement1.setString(6, "180000");
	 * statement1.setString(7, paLab.getBlood_sugar_fasting());
	 * statement1.setString(8, paLab.getHiv_status()); statement1.setInt(9,
	 * paLab.getRequest_id());
	 * 
	 * statement1.setString(1, "Referred To CM"); statement1.setInt(2, 10011);
	 * 
	 * 
	 * statement1.execute(); System.out.println("REsult HAi yeh");
	 * 
	 * // if(result==true) // { model.addAttribute("updatedMessage",
	 * "Your Details Are Updated Successfully"); }
	 * 
	 * } catch (ClassNotFoundException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (SQLException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * 
	 * return "SummaryPage3"; }
	 */

	@GetMapping("/summaryPostPaRequest2")
	public String summaryPersonal2(HttpSession session, PaRequest paRequest) {
		System.out.println(paRequest);
		// int requestId = (int) session.getAttribute("requestId");
		// System.out.println(requestId);

		System.out.println("SYSSSSSSSSSSSSSSSSSSSSSSoUTVBJHGJHG" + paRequest);

		try {
			// PreparedStatement statement =
			// MySqlCon.getConnection().prepareStatement("insert into pa_request set
			// Current_Status = ? where Request_ID = ? ");
			PreparedStatement statement = MySqlCon.getConnection().prepareStatement(
					"insert into comments_audit(Request_ID,Member_ID,Status,Comments,Time_Details) values (?,?,?,?,CURRENT_TIMESTAMP)",
					Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, paRequest.getRequest_id());
			statement.setInt(2, paRequest.getMember_id());
			// statement.setString(3, "To Be AutoApproved");
			statement.setString(3, "Request Created");
			// statement.setString(3, "Request to be auto approve");
			statement.setString(4, "Re-Submitted");

			statement.execute();

			/*
			 * statement.setInt(1,10011); statement.setInt(2, 125); statement.setString(3,
			 * "Referred To CM"); statement.setString(4, "Everything is fine");
			 * statement.execute();
			 */
			PreparedStatement statement1 = MySqlCon.getConnection().prepareStatement(
					"update  pa_request set pa_request.Current_Status = ?  where pa_request.Request_ID = ? ");

			// statement1.setString(1, "To Be AutoApproved");
			statement1.setString(1, "Request Created");
			// statement1.setString(1, "Request to be Auto approve");
			statement1.setInt(2, paRequest.getRequest_id());
			/*
			 * statement1.setString(1, "Referred To CM"); statement1.setInt(2, 10011);
			 */

			statement1.execute();
			System.out.println("REsult HAi yeh");

//				if(result==true)
//				{
			// model.addAttribute("updatedMessage","Your Details Are Updated Successfully"
			// );
			/* } */

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String inline = null;

		try {
			/*
			 * URL url =new
			 * URL("http://192.168.99.1:8001/preauth/allPersonalRequests/"+paRequest.
			 * getRequest_id());
			 */ // @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
			URL url = new URL("http://192.168.99.1:7001/preauth/allPersonalRequests/" + paRequest.getRequest_id());
			System.out.println("OBJect IDDDDDDDDDDDDDDDDDDDDDD555555555555" + paRequest.getRequest_id());

			try {
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("GET");
				// conn.setRequestProperty("Authorization","Bearer
				// AAIkZjFmNjAzNDctZDY0ZC00YjBkLWJhNDEtZTUxM2VkNGYyYzMw4GqEXQEzGn7k_GUWgkQgifw6YuAVIwr8UpTcGimvIEEnQvpyUh4hP85JAwwogki8Z-_vXf71dqlVHgAllTf5XHq__5ee1u6Iyjfuf83UZJz_4n51pELr4qrMvF3MxW-C5SlhsSQaU5vGuJXgh3g0dA");
				conn.setRequestProperty("Content-Type", "application/json");
				// conn.setRequestProperty("accept", "application/json");
				// System.out.println("Tokeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnndshfhdgsfhgdfshgdfs"+conn.getHeaderFields());
				conn.connect();
				int responsecode = conn.getResponseCode();
				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
					StringBuilder sb = new StringBuilder();
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					inline = sb.toString();
					// Scanner sc = new Scanner(url.openStream());
					// System.out.println("54666665757575757577777777777777777"+url.openStream().read());
					System.out.println("54666665757575757577777777777777777" + inline);
					/*
					 * while(sc.hasNext()) {
					 * 
					 * inline=inline+sc.nextLine(); }
					 * System.out.println("\nJSON data in string format");
					 * System.out.println(inline); sc.close();
					 */
				}
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		// JSONParser parse= new JSONParser();

		JSONParser jsonParser1 = new JSONParser();
		try {

			// You need to fix this part
			JSONObject obj1 = (JSONObject) jsonParser1.parse(inline);
			// System.out.println("JSOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONARRRRRRRRRRRRRRRRRRAAAAYYY"+obj);
			session.setAttribute("PersonalDetails", obj1);
			System.out.println("sahdgsjhgfjashgfdjsghdfjhsdgfjghsdfjghsj" + obj1);
			// model.addAttribute("jsonArray", jsonArray);

			// Iterator<?> i = jsonArray.iterator();

			/*
			 * while (i.hasNext()) { JSONObject obj1 = (JSONObject) i.next();
			 * System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"+obj1);
			 * // int tweetID = (int) obj.get("id"); //String lang = (String)
			 * obj.get("lang"); }
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "SummaryPage3";
	}

	@GetMapping("/returnHome")
	public String returnHome(HttpSession session) {
		return "redirect:/Provider/GoHome";
	}

	public String returnHome2(HttpSession session) {
		return "redirect:/Provider/GoHome";
	}

	@GetMapping("PostPaRequest/returnHome")
	public String returnHome3(HttpSession session) {
		return "redirect:/Provider/GoHome";
	}

	@ResponseBody
	@PostMapping("/getToken")
	public String getToken(HttpSession session) {
		String inline = null;

		String tokenEndpoint = "https://api.eu.apiconnect.ibmcloud.com/kalyanikatesyntelinccom-dev/sandbox/oauth-end/oauth2/token";
		// String APIURL="http://192.168.99.1:5006/preauth/memberUniData/"+searchId;

		String urlParameters = "client_id=78a6e449-54c5-43dd-a296-4c9cdfa71d7f&grant_type=client_credentials&client_secret=K4oM1pC7qV2cO8hP1cK7oG1gB4dJ6tT6tM5cG2yE3oV1uW1fO3&scope=view_branches";
		byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);
		int postDataLength = postData.length;

		try {

			URL tokenurl = new URL(tokenEndpoint);

			try {
				HttpURLConnection conn = (HttpURLConnection) tokenurl.openConnection();
				conn.setDoOutput(true);
				conn.setInstanceFollowRedirects(false);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				conn.setRequestProperty("charset", "utf-8");
				conn.setRequestProperty("Accept", "application/json");
				conn.setUseCaches(false);

				DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
				wr.write(postData);

				conn.connect();

				int responsecode = conn.getResponseCode();

				if (responsecode != 200)
					throw new RuntimeException("Its an Error!!! HttpResponseCode:" + responsecode);
				else {
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					// new BufferedReader(new InputStreamReader(tokenurl.openStream()));

					StringBuilder sb = new StringBuilder();

					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}

					inline = sb.toString();

					System.out.println("Here's the token......." + inline);
					// return inline;

				}

			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		String finalToken = null;
		JSONParser jsonParser = new JSONParser();
		JSONObject obj = null;

		try {

			obj = (JSONObject) jsonParser.parse(inline);
			session.setAttribute("jsonData", obj);
			finalToken = (String) obj.get("access_token");
			System.out.println("FInal token is:------" + finalToken);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return finalToken;

		// InsurerModel provider=(InsurerModel) session.getAttribute("user");
		// JSONObject obj4=(JSONObject) session.getAttribute("jsonData");

		// System.out.println(obj4.get("request_id"));

	}

}
