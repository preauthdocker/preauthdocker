package syntel.Provider.ProviderUI.model;

public class ProviderInfo {
	private int provider_id;
	private int request_id;
	private String provider_name;
	private String provider_type; 
	private String speciality;
	private int Phone_no;
	private String address;
	
	
	 public ProviderInfo() 
		 {
		super();
		
	}
	
	
	



	public ProviderInfo(int provider_id, int request_id, String provider_name, String provider_type, String speciality,
			int phone_no, String address) {
		super();
		this.provider_id = provider_id;
		this.request_id = request_id;
		this.provider_name = provider_name;
		this.provider_type = provider_type;
		this.speciality = speciality;
		Phone_no = phone_no;
		this.address = address;
	}


	public String getProvider_type() {
		return provider_type;
	}
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	public int getPhone_no() {
		return Phone_no;
	}
	public void setPhone_no(int phone_no) {
		Phone_no = phone_no;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}



	public String getProvider_name() {
		return provider_name;
	}



	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}



	public int getProvider_id() {
		return provider_id;
	}



	public void setProvider_id(int provider_id) {
		this.provider_id = provider_id;
	}
	@Override
	public String toString() {
		return "ProviderInfo [provider_id=" + provider_id + ", request_id=" + request_id + ", provider_name="
				+ provider_name + ", provider_type=" + provider_type + ", speciality=" + speciality + ", Phone_no="
				+ Phone_no + ", address=" + address + "]";
	} 

}
