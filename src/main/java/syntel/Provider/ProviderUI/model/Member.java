package syntel.Provider.ProviderUI.model;

import java.util.Date;

import javax.persistence.Id;

public class Member {
	
	@Id
	int memberId;
	String firstName;
	String lastName;
	String sex;
	int age;
	Date dob;
	Date lastDiagnosisDate;
	String insuredStatus;
	String lastReportingSource;
	int providerId;
	int payerId;
	int policyId;
	Date latestTreatmentInitiationDate;
	Date dateOfLastContact;
	String vitalStatus;
	String sourceOfVitalStatus;
	String deathCause;
	long contactNo;
	FamilyHistory history;
	public Member() {
		super();
	}
	public Member(int memberId, String firstName, String lastName, String sex, int age, Date dob,
			Date lastDiagnosisDate, String insuredStatus, String lastReportingSource, int providerId, int payerId,
			int policyId, Date latestTreatmentInitiationDate, Date dateOfLastContact, String vitalStatus,
			String sourceOfVitalStatus, String deathCause, long contactNo, FamilyHistory history) {
		super();
		this.memberId = memberId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.age = age;
		this.dob = dob;
		this.lastDiagnosisDate = lastDiagnosisDate;
		this.insuredStatus = insuredStatus;
		this.lastReportingSource = lastReportingSource;
		this.providerId = providerId;
		this.payerId = payerId;
		this.policyId = policyId;
		this.latestTreatmentInitiationDate = latestTreatmentInitiationDate;
		this.dateOfLastContact = dateOfLastContact;
		this.vitalStatus = vitalStatus;
		this.sourceOfVitalStatus = sourceOfVitalStatus;
		this.deathCause = deathCause;
		this.contactNo = contactNo;
		this.history = history;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public Date getLastDiagnosisDate() {
		return lastDiagnosisDate;
	}
	public void setLastDiagnosisDate(Date lastDiagnosisDate) {
		this.lastDiagnosisDate = lastDiagnosisDate;
	}
	public String getInsuredStatus() {
		return insuredStatus;
	}
	public void setInsuredStatus(String insuredStatus) {
		this.insuredStatus = insuredStatus;
	}
	public String getLastReportingSource() {
		return lastReportingSource;
	}
	public void setLastReportingSource(String lastReportingSource) {
		this.lastReportingSource = lastReportingSource;
	}
	public int getProviderId() {
		return providerId;
	}
	public void setProviderId(int providerId) {
		this.providerId = providerId;
	}
	public int getPayerId() {
		return payerId;
	}
	public void setPayerId(int payerId) {
		this.payerId = payerId;
	}
	public int getPolicyId() {
		return policyId;
	}
	public void setPolicyId(int policyId) {
		this.policyId = policyId;
	}
	public Date getLatestTreatmentInitiationDate() {
		return latestTreatmentInitiationDate;
	}
	public void setLatestTreatmentInitiationDate(Date latestTreatmentInitiationDate) {
		this.latestTreatmentInitiationDate = latestTreatmentInitiationDate;
	}
	public Date getDateOfLastContact() {
		return dateOfLastContact;
	}
	public void setDateOfLastContact(Date dateOfLastContact) {
		this.dateOfLastContact = dateOfLastContact;
	}
	public String getVitalStatus() {
		return vitalStatus;
	}
	public void setVitalStatus(String vitalStatus) {
		this.vitalStatus = vitalStatus;
	}
	public String getSourceOfVitalStatus() {
		return sourceOfVitalStatus;
	}
	public void setSourceOfVitalStatus(String sourceOfVitalStatus) {
		this.sourceOfVitalStatus = sourceOfVitalStatus;
	}
	public String getDeathCause() {
		return deathCause;
	}
	public void setDeathCause(String deathCause) {
		this.deathCause = deathCause;
	}
	public long getContactNo() {
		return contactNo;
	}
	public void setContactNo(long contactNo) {
		this.contactNo = contactNo;
	}
	public FamilyHistory getHistory() {
		return history;
	}
	public void setHistory(FamilyHistory history) {
		this.history = history;
	}
	@Override
	public String toString() {
		return "Member [memberId=" + memberId + ", firstName=" + firstName + ", lastName=" + lastName + ", sex=" + sex
				+ ", age=" + age + ", dob=" + dob + ", lastDiagnosisDate=" + lastDiagnosisDate + ", insuredStatus="
				+ insuredStatus + ", lastReportingSource=" + lastReportingSource + ", providerId=" + providerId
				+ ", payerId=" + payerId + ", policyId=" + policyId + ", latestTreatmentInitiationDate="
				+ latestTreatmentInitiationDate + ", dateOfLastContact=" + dateOfLastContact + ", vitalStatus="
				+ vitalStatus + ", sourceOfVitalStatus=" + sourceOfVitalStatus + ", deathCause=" + deathCause
				+ ", contactNo=" + contactNo + ", history=" + history + "]";
	}
	
	
	

}
