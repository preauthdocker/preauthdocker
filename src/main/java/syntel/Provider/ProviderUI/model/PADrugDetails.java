package syntel.Provider.ProviderUI.model;

public class PADrugDetails {

	private int drug_id;
	private int request_id;
	private String disease;
	private String drug_name;
	private String drug_substitute;
	private String max_dosage;

	private String strength;
	private String duration;
	private String clinical_info;
	private String allow_combination;
	private String comment;

	public PADrugDetails() {
		super();

	}

	public PADrugDetails(int drug_id, int request_id, String disease, String drug_name, String drug_substitute,
			String max_dosage, String strength, String duration, String clinical_info, String allow_combination,
			String comment) {
		super();
		this.drug_id = drug_id;
		this.request_id = request_id;
		this.disease = disease;
		this.drug_name = drug_name;
		this.drug_substitute = drug_substitute;
		this.max_dosage = max_dosage;

		this.strength = strength;
		this.duration = duration;
		this.clinical_info = clinical_info;
		this.allow_combination = allow_combination;
		this.comment = comment;
	}

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public int getDrug_id() {
		return drug_id;
	}

	public void setDrug_id(int drug_id) {
		this.drug_id = drug_id;
	}

	public int getRequest_id() {
		return request_id;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public String getDrug_name() {
		return drug_name;
	}

	public void setDrug_name(String drug_name) {
		this.drug_name = drug_name;
	}

	public String getDrug_substitute() {
		return drug_substitute;
	}

	public void setDrug_substitute(String drug_substitute) {
		this.drug_substitute = drug_substitute;
	}

	public String getMax_dosage() {
		return max_dosage;
	}

	public void setMax_dosage(String max_dosage) {
		this.max_dosage = max_dosage;
	}

	public String getAllow_combination() {
		return allow_combination;
	}

	public void setAllow_combination(String allow_combination) {
		this.allow_combination = allow_combination;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getClinical_info() {
		return clinical_info;
	}

	public void setClinical_info(String clinical_info) {
		this.clinical_info = clinical_info;
	}

	@Override
	public String toString() {
		return "PADrugDetails [drug_id=" + drug_id + ", request_id=" + request_id + ", disease=" + disease
				+ ", drug_name=" + drug_name + ", drug_substitute=" + drug_substitute + ", max_dosage=" + max_dosage
				+ ", strength=" + strength + ", duration=" + duration + ", clinical_info=" + clinical_info
				+ ", allow_combination=" + allow_combination + ", comment=" + comment + "]";
	}

}
