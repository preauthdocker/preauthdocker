package syntel.Provider.ProviderUI.model;

import java.io.Serializable;
import java.sql.Date;


//import java.sql.Date;

public class PaRequest {
	
	
	private int request_id;
	private int member_id;
	private String first_name;
	private String last_name;
	private String sex;
	private int age;
	private Date dob;
	private String allergies;
	private String insured;
	private int payer_id;
	private int policy_id;
	private Date latest_treatment_date;
	private String vital_status;
	private String last_reporting_source;
	private int provider_id;
	private String current_status;
	private String comment;
	
	
	
	public PaRequest() {
		super();	
	}

	public PaRequest(int request_id, int member_id, String first_name, String last_name, String sex, int age, Date dob,
			String allergies, String insured, int payer_id, int policy_id, Date latest_treatment_date,
			String vital_status, String last_reporting_source, int provider_id, String current_status, String comment) {
		super();
		this.request_id = request_id;
		this.member_id = member_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.sex = sex;
		this.age = age;
		this.dob = dob;
		this.allergies = allergies;
		this.insured = insured;
		this.payer_id = payer_id;
		this.policy_id = policy_id;
		this.latest_treatment_date = latest_treatment_date;
		this.vital_status = vital_status;
		this.last_reporting_source = last_reporting_source;
		this.provider_id = provider_id;
		this.current_status = current_status;
		this.comment = comment;
	}

	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getRequest_id() {
		return request_id;
	}

	public String getAllergies() {
		return allergies;
	}

	public void setAllergies(String allergies) {
		this.allergies = allergies;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public int getMember_id() {
		return member_id;
	}

	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getInsured() {
		return insured;
	}

	public void setInsured(String insured) {
		this.insured = insured;
	}

	public int getPayer_id() {
		return payer_id;
	}

	public void setPayer_id(int payer_id) {
		this.payer_id = payer_id;
	}

	public int getPolicy_id() {
		return policy_id;
	}

	public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}

	public Date getLatest_treatment_date() {
		return latest_treatment_date;
	}

	public void setLatest_treatment_date(Date latest_treatment_date) {
		this.latest_treatment_date = latest_treatment_date;
	}

	public String getVital_status() {
		return vital_status;
	}

	public void setVital_status(String vital_status) {
		this.vital_status = vital_status;
	}

	public String getLast_reporting_source() {
		return last_reporting_source;
	}

	public void setLast_reporting_source(String last_reporting_source) {
		this.last_reporting_source = last_reporting_source;
	}

	public int getProvider_id() {
		return provider_id;
	}

	public void setProvider_id(int provider_id) {
		this.provider_id = provider_id;
	}

	public String getCurrent_status() {
		return current_status;
	}

	public void setCurrent_status(String current_status) {
		this.current_status = current_status;
	}

	@Override
	public String toString() {
		return "PaRequest [request_id=" + request_id + ", member_id=" + member_id + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", sex=" + sex + ", age=" + age + ", dob=" + dob + ", allergies="
				+ allergies + ", insured=" + insured + ", payer_id=" + payer_id + ", policy_id=" + policy_id
				+ ", latest_treatment_date=" + latest_treatment_date + ", vital_status=" + vital_status
				+ ", last_reporting_source=" + last_reporting_source + ", provider_id=" + provider_id
				+ ", current_status=" + current_status + ", comment=" + comment + "]";
	}

	
	

	
	
}