package syntel.Provider.ProviderUI.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * The persistent class for the provider database table.
 * 
 */
@Entity
@NamedQuery(name="Provider.findAll", query="SELECT p FROM Provider p")
public class Provider implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int provider_ID;

	private String provider_Name;
	
	private String last_name;

	private String provider_Pass;
	
	private String provider_city;
	
	private Date provider_dob;
	
	private String provider_sex;
	
	private String provider_type;
	
	private String speciality;
	
	private String phone_no;
	
	private String address;
	
	private int zipcode;
	

	public Provider() {
	}
	


	


	public Provider(int provider_ID, String provider_Name, String last_name, String provider_Pass, String provider_city,
			Date provider_dob, String provider_sex, String provider_type, String speciality, String phone_no,
			String address, int zipcode) {
		super();
		this.provider_ID = provider_ID;
		this.provider_Name = provider_Name;
		this.last_name = last_name;
		this.provider_Pass = provider_Pass;
		this.provider_city = provider_city;
		this.provider_dob = provider_dob;
		this.provider_sex = provider_sex;
		this.provider_type = provider_type;
		this.speciality = speciality;
		this.phone_no = phone_no;
		this.address = address;
		this.zipcode = zipcode;
	}



	public String getLast_name() {
		return last_name;
	}






	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}






	public String getSpeciality() {
		return speciality;
	}



	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}



	public String getPhone_no() {
		return phone_no;
	}



	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public int getZipcode() {
		return zipcode;
	}



	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}



	public int getProvider_ID() {
		return provider_ID;
	}


	public String getProvider_type() {
		return provider_type;
	}
	public void setProvider_type(String provider_type) {
		this.provider_type = provider_type;
	}
	public void setProvider_ID(int provider_ID) {
		this.provider_ID = provider_ID;
	}


	public String getProvider_Name() {
		return provider_Name;
	}


	public void setProvider_Name(String provider_Name) {
		this.provider_Name = provider_Name;
	}


	public String getProvider_Pass() {
		return provider_Pass;
	}


	public void setProvider_Pass(String provider_Pass) {
		this.provider_Pass = provider_Pass;
	}


	public String getProvider_city() {
		return provider_city;
	}


	public void setProvider_city(String provider_city) {
		this.provider_city = provider_city;
	}


	public Date getProvider_dob() {
		return provider_dob;
	}


	public void setProvider_dob(Date provider_dob) {
		this.provider_dob = provider_dob;
	}


	public String getProvider_sex() {
		return provider_sex;
	}


	public void setProvider_sex(String provider_sex) {
		this.provider_sex = provider_sex;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}






	@Override
	public String toString() {
		return "Provider [provider_ID=" + provider_ID + ", provider_Name=" + provider_Name + ", last_name=" + last_name
				+ ", provider_Pass=" + provider_Pass + ", provider_city=" + provider_city + ", provider_dob="
				+ provider_dob + ", provider_sex=" + provider_sex + ", provider_type=" + provider_type + ", speciality="
				+ speciality + ", phone_no=" + phone_no + ", address=" + address + ", zipcode=" + zipcode + "]";
	}



	

	




}