package syntel.Provider.ProviderUI.model;

import java.sql.Timestamp;

public class CommentDetails {


	int index;
	int request_id;
	int member_id;
	String status;
	String comments;
	Timestamp time_details;
	public CommentDetails() {
		super();
	}
	public CommentDetails(int index, int request_id, int member_id, String status, String comments,
			Timestamp time_details) {
		super();
		this.index = index;
		this.request_id = request_id;
		this.member_id = member_id;
		this.status = status;
		this.comments = comments;
		this.time_details = time_details;
	}
	
	
	
	
	public CommentDetails(int request_id, int member_id, String status, String comments, Timestamp time_details) {
		super();
		this.request_id = request_id;
		this.member_id = member_id;
		this.status = status;
		this.comments = comments;
		this.time_details = time_details;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	public int getMember_id() {
		return member_id;
	}
	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Timestamp getTime_details() {
		return time_details;
	}
	public void setTime_details(Timestamp time_details) {
		this.time_details = time_details;
	}
	@Override
	public String toString() {
		return "CommentDetails [index=" + index + ", request_id=" + request_id + ", member_id=" + member_id
				+ ", status=" + status + ", comments=" + comments + ", time_details=" + time_details + "]";
	}
	
	
	
	
}
