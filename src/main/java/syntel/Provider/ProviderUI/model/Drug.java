package syntel.Provider.ProviderUI.model;

import java.util.ArrayList;
import java.util.List;

public class Drug {
	
	private List<PADrugDetails> drugDetails = new ArrayList<PADrugDetails>();

	
	public Drug() {
		super();
	}
	
	
	public Drug(List<PADrugDetails> drugDetails) {
		super();
		this.drugDetails = drugDetails;
	}
	
	
	public List<PADrugDetails> getDrugDetails() {
		return drugDetails;
	}

	public void setDrugDetails(List<PADrugDetails> drugDetails) {
		this.drugDetails = drugDetails;
	}


	@Override
	public String toString() {
		return "Drug [drugDetails=" + drugDetails + "]";
	}

	

	
	

}
