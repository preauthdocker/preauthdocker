package syntel.Provider.ProviderUI.model;

public class FamilyHistory {
	
	int familyId;
	int memberId;
	String diabetes;
	String hypertension;
	String osteoarthritis;
	String hivOrStd;
	String heartDisease;
	String alcoholOrDrugAbuse;
	public FamilyHistory() {
		super();
	}
	public FamilyHistory(int familyId, int memberId, String diabetes, String hypertension, String osteoarthritis,
			String hivOrStd, String heartDisease, String alcoholOrDrugAbuse) {
		super();
		this.familyId = familyId;
		this.memberId = memberId;
		this.diabetes = diabetes;
		this.hypertension = hypertension;
		this.osteoarthritis = osteoarthritis;
		this.hivOrStd = hivOrStd;
		this.heartDisease = heartDisease;
		this.alcoholOrDrugAbuse = alcoholOrDrugAbuse;
	}
	public int getFamilyId() {
		return familyId;
	}
	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getDiabetes() {
		return diabetes;
	}
	public void setDiabetes(String diabetes) {
		this.diabetes = diabetes;
	}
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getOsteoarthritis() {
		return osteoarthritis;
	}
	public void setOsteoarthritis(String osteoarthritis) {
		this.osteoarthritis = osteoarthritis;
	}
	public String getHivOrStd() {
		return hivOrStd;
	}
	public void setHivOrStd(String hivOrStd) {
		this.hivOrStd = hivOrStd;
	}
	public String getHeartDisease() {
		return heartDisease;
	}
	public void setHeartDisease(String heartDisease) {
		this.heartDisease = heartDisease;
	}
	public String getAlcoholOrDrugAbuse() {
		return alcoholOrDrugAbuse;
	}
	public void setAlcoholOrDrugAbuse(String alcoholOrDrugAbuse) {
		this.alcoholOrDrugAbuse = alcoholOrDrugAbuse;
	}
	@Override
	public String toString() {
		return "FamilyHistory [familyId=" + familyId + ", memberId=" + memberId + ", diabetes=" + diabetes
				+ ", hypertension=" + hypertension + ", osteoarthritis=" + osteoarthritis + ", hivOrStd=" + hivOrStd
				+ ", heartDisease=" + heartDisease + ", alcoholOrDrugAbuse=" + alcoholOrDrugAbuse + "]";
	}
	
	

}
