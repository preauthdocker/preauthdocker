package syntel.Provider.ProviderUI.model;

public class PALabReports {
	
	private int request_id;
	private String blood_pressure;
	private String diastolic;
	private String systolic;
	private String heamoglobin;
	private String wbc;
	private String rbc;
	
	private String blood_sugar_fasting;
	private String hiv_status;
	public PALabReports() {
		super();
	}
	public PALabReports(int request_id, String blood_pressure, String diastolic, String systolic, String heamoglobin,
			String wbc, String rbc,  String blood_sugar_fasting, String hiv_status) {
		super();
		this.request_id = request_id;
		this.blood_pressure = blood_pressure;
		this.diastolic = diastolic;
		this.systolic = systolic;
		this.heamoglobin = heamoglobin;
		this.wbc = wbc;
		this.rbc = rbc;
		this.blood_sugar_fasting = blood_sugar_fasting;
		this.hiv_status = hiv_status;
	}
	public int getRequest_id() {
		return request_id;
	}
	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}
	public String getBlood_pressure() {
		return blood_pressure;
	}
	public void setBlood_pressure(String blood_pressure) {
		this.blood_pressure = blood_pressure;
	}
	public String getDiastolic() {
		return diastolic;
	}
	public void setDiastolic(String diastolic) {
		this.diastolic = diastolic;
	}
	public String getSystolic() {
		return systolic;
	}
	public void setSystolic(String systolic) {
		this.systolic = systolic;
	}
	public String getHeamoglobin() {
		return heamoglobin;
	}
	public void setHeamoglobin(String heamoglobin) {
		this.heamoglobin = heamoglobin;
	}
	public String getWbc() {
		return wbc;
	}
	public void setWbc(String wbc) {
		this.wbc = wbc;
	}
	public String getRbc() {
		return rbc;
	}
	public void setRbc(String rbc) {
		this.rbc = rbc;
	}
	
	public String getBlood_sugar_fasting() {
		return blood_sugar_fasting;
	}
	public void setBlood_sugar_fasting(String blood_sugar_fasting) {
		this.blood_sugar_fasting = blood_sugar_fasting;
	}
	public String getHiv_status() {
		return hiv_status;
	}
	public void setHiv_status(String hiv_status) {
		this.hiv_status = hiv_status;
	}
	@Override
	public String toString() {
		return "PALabReports [request_id=" + request_id + ", blood_pressure=" + blood_pressure + ", diastolic="
				+ diastolic + ", systolic=" + systolic + ", heamoglobin=" + heamoglobin + ", wbc=" + wbc + ", rbc="
				+ rbc +  ", blood_sugar_fasting=" + blood_sugar_fasting + ", hiv_status="
				+ hiv_status + "]";
	}
	

}
