package syntel.Provider.ProviderUI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProviderUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProviderUiApplication.class, args);
	}
}
