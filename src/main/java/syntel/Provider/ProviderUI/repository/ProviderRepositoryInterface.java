package syntel.Provider.ProviderUI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import syntel.Provider.ProviderUI.model.Provider;

@Repository
public interface ProviderRepositoryInterface extends JpaRepository<Provider, Integer> {

	@Query("select p from Provider p where p.provider_Name= ?1 and p.provider_Pass= ?2")
	Provider findProviderByNameAndPass(String name, String password);
}
