package syntel.Provider.ProviderUI.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import syntel.Provider.ProviderUI.entity.DrugDetailsEntity;

public interface DrugDetailsRepository extends JpaRepository<DrugDetailsEntity, Integer> {
	
	@Query(value = "SELECT *"
			+ " FROM pa_drug_details"
			+ " WHERE pa_drug_details.Drug_Name = ?", nativeQuery = true)
	public List<DrugDetailsEntity> getDrugsByName(String drugName);

}
