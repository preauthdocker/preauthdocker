package syntel.Provider.ProviderUI.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import syntel.Provider.ProviderUI.entity.DrugDetailsEntity;
import syntel.Provider.ProviderUI.repository.DrugDetailsRepository;
import syntel.Provider.ProviderUI.service.DrugService;

@Service
public class DrugDetailsImplementation implements DrugService {

	@Autowired
	DrugDetailsRepository drugRepo;

	@Override
	public List<DrugDetailsEntity> getDrugDetailsByName(String drugName) {

		List<DrugDetailsEntity> drugNameList = drugRepo.getDrugsByName(drugName);
		return drugNameList;
	}

}
