package syntel.Provider.ProviderUI.service;

import java.util.List;

import syntel.Provider.ProviderUI.entity.DrugDetailsEntity;

public interface DrugService{

	public List<DrugDetailsEntity> getDrugDetailsByName(String drugName);
}
