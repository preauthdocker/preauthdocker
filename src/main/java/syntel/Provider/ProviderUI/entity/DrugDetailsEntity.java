package syntel.Provider.ProviderUI.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: DrugDetailsEntity
 *
 */
@Entity
@Table(name = "pa_drug_details")
public class DrugDetailsEntity implements Serializable {

	@Id
	@Column(name = "Drug_ID")
	private int drug_id;

	@Column(name = "Request_ID")
	private int request_id;
	@Column(name = "Disease")
	private String disease;
	@Column(name = "Drug_Name")
	private String drug_name;
	@Column(name = "Drug_Substitute")
	private String drug_substitute;
	@Column(name = "Max_Dosage")
	private String max_dosage;
	@Column(name = "Strength")
	private String strength;
	@Column(name = "Duration")
	private String duration;
	@Column(name = "Clinical_info")
	private String clinical_info;
	@Column(name = "Allow_Combination")
	private String allow_combination;
	@Column(name = "Drug_Comments")
	private String comment;

	public DrugDetailsEntity() {
		super();
	}
	
	public DrugDetailsEntity(int drug_id, int request_id, String disease, String drug_name, String drug_substitute,
			String max_dosage, String strength, String duration, String clinical_info, String allow_combination,
			String comment) {
		super();
		this.drug_id = drug_id;
		this.request_id = request_id;
		this.disease = disease;
		this.drug_name = drug_name;
		this.drug_substitute = drug_substitute;
		this.max_dosage = max_dosage;
		this.strength = strength;
		this.duration = duration;
		this.clinical_info = clinical_info;
		this.allow_combination = allow_combination;
		this.comment = comment;
	}



	public int getDrug_id() {
		return drug_id;
	}

	public void setDrug_id(int drug_id) {
		this.drug_id = drug_id;
	}

	public int getRequest_id() {
		return request_id;
	}

	public void setRequest_id(int request_id) {
		this.request_id = request_id;
	}

	public String getDisease() {
		return disease;
	}

	public void setDisease(String disease) {
		this.disease = disease;
	}

	public String getDrug_name() {
		return drug_name;
	}

	public void setDrug_name(String drug_name) {
		this.drug_name = drug_name;
	}

	public String getDrug_substitute() {
		return drug_substitute;
	}

	public void setDrug_substitute(String drug_substitute) {
		this.drug_substitute = drug_substitute;
	}

	public String getMax_dosage() {
		return max_dosage;
	}

	public void setMax_dosage(String max_dosage) {
		this.max_dosage = max_dosage;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getClinical_info() {
		return clinical_info;
	}

	public void setClinical_info(String clinical_info) {
		this.clinical_info = clinical_info;
	}

	public String getAllow_combination() {
		return allow_combination;
	}

	public void setAllow_combination(String allow_combination) {
		this.allow_combination = allow_combination;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private static final long serialVersionUID = 1L;

}
