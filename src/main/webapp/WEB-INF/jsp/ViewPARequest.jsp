<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>All PA Requests</title>
<meta charset="UTF-8">
<meta http-equiv="refresh" content="30">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script
	src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>




<style>
/*Loader start*/
.loader {
  border: 10px solid #f3f3f3;
  border-radius: 50%;
  border-top: 10px solid #3498db;
  width: 20px;
  height: 20px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
/*Loader end*/
.dataTables_info, .dataTables_LengthChange {
	display: none;
}

html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif
}

/* body, html {
  height: 200%;
  margin: 0;
} */

/* .signup-form {
	width: 780px;
	margin: 0 auto;
	padding: 10px 0;
} */
.signup-form {
	width: 1025px;
	height: 785px;
	margin-left: 15px;
	padding: 5px 0;
}

#Details {
	/* font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; */
	font-family: "Roboto", Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
}

#Details td, #Details th {
	border: 0px solid #ddd;
	padding: 8px;
}

#Details tr:nth-child(even) {
	background-color: #f2f2f2;
	align: center;
}

#Details tr:hover {
	background-color: #ddd;
}

th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: center;
	background-color: #6e8d9e;
	color: white;
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
}

body, html {
	height: 180%;
}

.login-form .form-control {
	background: #f7f7f7 none repeat scroll 0 0;
	border: 1px solid #d4d4d4;
	border-radius: 4px;
	font-size: 14px;
	height: 50px;
	line-height: 50px;
	padding: 0px 0;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}

#bg-image img {
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	max-height: 100%;
}

#bg-image {
	height: 100%;
	width: 100%;
	float: left;
	overflow: hidden;
	position: absolute;
}

#headerTab2 {
	background-color: #006398;
}

.contactImageStyle {
	float: right;
	position: relative;
	right: 80px;
	top: 30px;
}

.contactNumberStyle {
	position: relative;
	left: 45px;
	bottom: 35px;
}
</style>
<script>
	$(document).ready(
			function() {
				
				$('#Details').DataTable({
					"language" : {
						"paginate" : {
							'previous' : '&lt',
							'next' : '&gt'
						}
					},

					"bLengthChange" : false,
					 "stateSave": true

				});
				

			});
	
	
	
	
	function refresh(){
		
		
		$.ajax({
		    	type:"GET",
		    	dataType: "json",
		    	url: "http://localhost:7001/preauth/allProviderRequests/"+${Provider.provider_ID}		
		    }).then(function(data) {	    	
		    	
		    	// populateDataTable(data);
		  		
		    	
		    });
			
			
		/* $.ajax({
	    	type:"GET",
	    	url: "http://localhost:7001/preauth/allProviderRequests/"+${Provider.provider_ID}		
	    }).then(function(data) {	    	
	    	
	    	jsonArray = data;
	  
	    	
	    }); */
		
	}
	
function populateDataTable(data){
	alert("In populateDataTable"+data[0].request_id)
	 $("#Details").DataTable().clear();
	// for(var i=0;i<data.length; i++)
		// {
		var myTable=$('#Details').DataTable({
			"language" : {
				"paginate" : {
					'previous' : '&lt',
					'next' : '&gt'
				}
			},
				
			"bLengthChange" : false,
			 "stateSave": true,
		/* 	 "columns": [{
				    "data": "request_id",
				    "data": "request_id",
				    "data": "request_id",
				    "data": "request_id",
				    "data": "request_id",
				    "data": "request_id",
				   	"data": "request_id"
			 }]
 */
		});
		
		    				
		
		
		/* 
        var result=[];
        for(var i=0;i<data.length;i++)
        	{
            result.push(data[i].request_id);
            result.push(data[i].request_id);
            result.push(data[i].request_id);
            result.push(data[i].request_id);
            result.push(data[i].request_id);
            result.push(data[i].request_id);
            result.push(data[i].request_id); 
        	}
        alert("In populateDataTable"+result)
        myTable.rows.add(result);
        myTable.draw(); */
		// }
		/* $.ajax({
	    	type:"GET",
	    	url: "http://localhost:7001/preauth/allProviderRequests/"+${Provider.provider_ID}		
	    }).then(function(data) {	    	
	    	
	    	jsonArray = data;
	  
	    	
	    }); */
		
	}
		
	function createLink() {
		var aTag = document.createElement('a');
		aTag.href = "";
		aTag.innerHTML = "Edit";
		aTag.id = "Link1"
		document.getElementById("heading").appendChild(aTag);
	}
	
	function forLoaderStatus() {
		 for (i = 0; i < document.getElementsByClassName("Status").length; i++) {
			if ((document.getElementsByClassName("Status")[i].innerHTML
					.includes("Request Created"))) {
				$('.loader')[i].style.display = "block";
			} else {
				$('.loader')[i].style.display = "none";
			}		
		
			
		} 
		
	}
	setInterval(forLoaderStatus, 1000);
	 
	
	
	
</script>

<!-- Pagination Script -->
<script>
	/* $(document).ready(function() {
	 $("li").click(function() {
	 var a = $(this).attr("value");
	 alert("a ki value" + a);
	 console.log("value li " + a);
	 data(a);
	 });
	 });

	 function data(a) {
	 var output = "";
	 for (i = ((a - 1) * 10); i < (a * 10); i++) {
	 output += '<tr>' + '<td>' + udata[i].userId + '</td>' + '<td>'
	 + udata[i].id + '</td>' + '<td>' + udata[i].title + '</td>'
	 + '<br>'
	 '</tr>';
	 }
	 document.getElementById('user').innerHTML = output;
	 } */
</script>
<!-- Pagination Script End -->

</head>

<body class="w3-theme-l5" id="LoginForm">

	<!-- Navbar -->
	<div class="w3-top">
		<%@ include file="header.jsp"%>
	</div>

	<!-- Page Container -->
	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">

		<div class="contactImageStyle">
			<img src="/images/icons/call-icon.png" />
			<div class="contactNumberStyle">
				Contact Insurer <br>+1-987-654-3210
			</div>
		</div>
		<!-- The Grid -->
		<div class="w3-row">
		<div class="w3-col m8" style="margin-top: -8px;">
				<div class="signup-form" align="center"
					style="width: 145%; margin-left: 15px;">
					<div class="w3-container w3-card w3-white w3-round w3-margin"
						align="center">
						</br>

						<!-- <table class="table table-hover table table-bordered" align="center"> -->
						<table id="Details" class="table table-striped table-bordered"
							style="width: 100%;">
							<thead>
								<tr>
									<th align="justify" style="background-color: #76a0b7; text-align:left">PA
										Request</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">Member
										ID</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">First
										Name</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">Last
										Name</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">Insured
										Status</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">Policy
										ID</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">Treatment
										Date</th>
									<th align="justify" style="background-color: #76a0b7;text-align:left">Status</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="i" items="${jsonArray}">
									<tr>
										<td align="left" class="viewRequestID">${i.request_id}
										</td>
										<td align="left" class="viewMemberId">${i.member_id}</td>
										<td align="left" class="viewFirstName">${i.first_name}</td>
										<td align="left" class="viewlastName">${i.last_name}</td>
										<td align="left" class="viewInsured">${i.insured}</td>
										<td align="left" class="viewPolicyId">${i.policy_id}</td>
										<td align="left" class="viewTreatmentDate">${i.latest_treatment_date}</td>
										<td class=Status id="currentStatus" align="left"><div
												class="loader" id="loader"></div>${i.current_status}</td>

									</tr>
								</c:forEach>
							</tbody>
						</table>

						</br>
						
					</div>
				</div>
			</div>


			<br> <br> <br>
		</div>

		<%@ include file="footer.jsp"%>
	</div>
	<!-- End Page Container -->
</body>
</html>
