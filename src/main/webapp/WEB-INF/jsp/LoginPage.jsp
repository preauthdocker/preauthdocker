<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Provider Portal-Log In</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
#LoginForm {
	background-image: url("/images/icons/PreAuth_login_BG.jpg");
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 190px;
	height: 900px;
}

.form-heading {
	color: #fff;
	font-size: 23px;
}

.panel h2 {
	color: #444444;
	font-size: 18px;
	margin: 0 0 8px 0;
}

.panel p {
	color: #777777;
	font-size: 14px;
	margin-bottom: 30px;
	line-height: 24px;
}

.login-form .form-control {
	background: #f7f7f7 none repeat scroll 0 0;
	border: 1px solid #d4d4d4;
	border-radius: 4px;
	font-size: 14px;
	height: 50px;
	line-height: 50px;
}

.main-div {
	background: #ffffff none repeat scroll 0 0;
	border-radius: 2px;
	margin: 10px auto 30px;
	max-width: 48%;
	padding: 70px 70px 70px 71px;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	min-width: 160px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

.dropdown-content a:hover {
	background-color: #f1f1f1
}

.dropdown:hover .dropdown-content {
	display: block;
}

.dropdown:hover .dropbtn {
	background-color: lightblue;
}

.login-form .form-group {
	margin-bottom: 10px;
}

.login-form {
	text-align: center;
}

.forgot a {
	color: #777777;
	font-size: 14px;
	text-decoration: underline;
}

.login-form  .btn.btn-primary {
	background: #f0ad4e none repeat scroll 0 0;
	border-color: #f0ad4e;
	color: #ffffff;
	font-size: 14px;
	width: 100%;
	height: 50px;
	line-height: 50px;
	padding: 0;
}

.forgot {
	text-align: left;
	margin-bottom: 30px;
}

.botto-text {
	color: #ffffff;
	font-size: 14px;
	margin: auto;
}

.login-form .btn.btn-primary.reset {
	background: #ff9900 none repeat scroll 0 0;
}

.back {
	text-align: left;
	margin-top: 10px;
}

.back a {
	color: #444444;
	font-size: 13px;
	text-decoration: none;
}

footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: grey;
	color: white;
}

#headerTab2 {
	background-color: #006398;
}

#SR {
	background-image: url('/images/icons/id_icon.png');
	position: left;
	padding-left: 45px;
	background-size: 50px;
	background-repeat: no-repeat;
	margin-top: 10px;

	/* border-radius:8px; */
}

#SP {
	background-image: url('/images/icons/password_icn.png');
	position: left;
	padding-left: 45px;
	background-size: 50px;
	background-repeat: no-repeat;
	margin-top: 10px;

	/* border-radius:8px; */
}
</style>
</head>
<body id="LoginForm">
	<div class="container" align=center style="margin-top: 100px">
		<!-- <div class="login-form"> -->
		<!-- <div class="main-div"> -->
		<div class="panel">
			<h1 align="center"
				style="color: #ffffff; font-style: Myriad; font-size: 30px">LOGIN</h1>
			<p></p>
		</div>
		<!--    <p class="w3-center">
		<img src="/images/avatar1.png" class="w3-circle"
		style="height: 106px; width: 106px" alt="Avatar">
	</p> -->
		<form id="Login" align=center action="Login" method="post"
			style="width: 300px;">

			<div class="form-group" id="SR">
				<input type="text" name="provider_Name" class="form-control"
					style="border: none; height: 50px;" id="inputEmail"
					placeholder="Enter Username">
			</div>
			<div class="form-group" id="SP">
				<input type="password" name="provider_Pass" class="form-control"
					style="border: none; height: 50px;" id="inputPassword"
					placeholder="Password">
			</div>

			<!--         <div class="forgot" align="right">
        <a href="reset.html">Forgot password</a>
        </div> -->
			<br>
			<button type="submit" class="btn btn-primary"
				style="background-color: #006398; border: none; height: 50px; width: 300px; font-style: Myriad; font-size: 22px">Login</button>

		</form>
		<!-- </div> -->
		<!-- </div> -->
		<br>
		<br>
	</div>
	<!-- <div style="background-color: orange; color: blue">
			<footer class="w3-container w3-theme-d3">
				<h5>PreAuth</h5>
				<p>
					Powered by <a href="https://www.atos-syntel.net" target="_blank">AtoS|Syntel</a>
				</p>
			</footer>
		</div> -->

</body>

</html>