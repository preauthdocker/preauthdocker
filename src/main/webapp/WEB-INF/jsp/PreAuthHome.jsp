<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>PreAUth Home DashBoard</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.anychart.com/js/8.0.1/anychart-core.min.js"></script>
<script src="https://cdn.anychart.com/js/8.0.1/anychart-pie.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif;
}

body {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	color: #ededed;
	font-family: 'Roboto', sans-serif;
}
</style>

<body class="w3-theme-l5">

	<div id="container" style="width: 500px; height: 500px"></div>
	<div><%@ include file="footer.jsp"%></div>
	<script>
		anychart.onDocumentReady(function() {

			// set the data
			var data = [ {
				x : "Approved",
				value : 60
			}, {
				x : "AutoApproved",
				value : 10
			}, {
				x : "Rejected",
				value : 20
			}, {
				x : "Pending",
				value : 10
			}, ];

			// create the chart
			var chart = anychart.pie();

			// set the chart title
			chart.title("Pre-Authorization requests");

			// add the data
			chart.data(data);

			// display the chart in the container
			chart.container('container');
			chart.draw();

		});
		
		function getRequestStatus(){
			$.ajax({
				type : "GET",
				url : "http://localhost:7002/preauth/drugnamerequest",
				dataType : "json",
				success : function(data) {
					for (var i = 0; i < data.length; i++) {
						if(data[i]!=null){
						var opt = new Option(data[i]);
						$("#dropDown").append(opt);
						}}
				},
				error : function(request, textStatus, errorThrown) {
					
				}
			})
		}
	</script>
</body>
</html>
