<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>Summary Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
}

body {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	color: #ededed;
	font-family: 'Roboto', sans-serif;
}

.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}

.form-control:focus {
	border-color: #5cb85c;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 780px;
	margin: 0 auto;
	padding: 20px 0;
}

.signup-form {
	width: 1010px;
	margin-left: 15px;
	padding: 20px 0;
}

.signup-form h3 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form h3:before, .signup-form h3:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
	min-width: 100px;
	max-width: 200px;
	outline: none !important;
}

.button {
	background-color: #4CAF50; /* Green */
	border: none;
	color: white;
	padding: 15px 32px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
}

.signup-form h3:before {
	left: 0;
}

.signup-form h3:after {
	right: 0;
}

/* .signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
} */
.signup-form form {
	color: #808080;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 22px;
}

/* .signup-form .form-group {
	margin-bottom: 7px;
} */
.signup-form input[type="checkbox"] {
	margin-top: 3px;
}

/* .signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}



.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
} */
.footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}
.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}
.login-form .form-control {
	background: #f7f7f7 none repeat scroll 0 0;
	border: 1px solid #d4d4d4;
	border-radius: 4px;
	font-size: 14px;
	height: 50px;
	line-height: 50px;
}

#headerTab2 {
	background-color: #006398;
}
.contactImageStyle {
	float: right;
	position: relative;
	right: 80px;
	top: 30px;
}

.contactNumberStyle {
	position: relative;
	left: 50px;
	bottom: 35px;
}
</style>

<SCRIPT>
	function backWithDelete() {
		//document.getElementById("backForm").submit();

		history.go(-1);

	}
</SCRIPT>

<body class="w3-theme-l5" id="LoginForm">

	<!-- Navbar -->
	<div class="w3-top">
		
			<%-- <a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
				href="javascript:void(0);" onclick="openNav()"><i
				class="fa fa-bars"></i></a> <a href="returnHome"
				style="color: #ffffff;"><i
				class="fa fa-home w3-margin-right"></i>PreAuth</a> <!-- <a href=""
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="Mails"><i class="fa fa-envelope"></i></a> -->
			<div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" title="Notifications">
					<i class="fa fa-bell"></i><span
						class="w3-badge w3-right w3-small w3-green">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
			<!--  <a
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="News"><i class="fa fa-newspaper-o"></i></a> --> <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				title="logout"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a href="#"
				class=" w3-right" style="color: #ffffff;"> <i
				class="fa w3-margin-right"></i>Welcome ${Provider.provider_Name}
			</a> --%>
			<%@ include file="header.jsp"%>
		</div>
	

	<!-- Page Container -->
	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">
		<!-- The Grid -->
		<div class="contactImageStyle">
			<img src="/images/icons/call-icon.png" />
			<div class="contactNumberStyle">
				Contact Insurer <br>+1-987-654-3210
			</div>
		</div>
		<div class="w3-row">
			<div class="w3-col m8" style="margin-top:-8px">
				<div class="signup-form" style="width:145%; margin-left:15px">
				<!-- <a href="returnHome" style="float:left;  padding-left:1px; margin-top:-85px; font-size:15px;"><font color="#0000ff">Back to Home</font></a> -->
					<form action="summaryPostPaRequest" method="get">
						<div class="w3-button w3-block"
						style="background-color: #76a0b7; color: #ffffff; height: 40px;">
						<p style="padding-bottom: 6px; margin-top:-3px;font-size:20px">Personal Details</p>
					</div>
						<br>
						<div class="form-group">
							<div class="row" style=" margin-left:3px">
								<div class="col-xs-3">
									Member ID: <p><label>${PersonalDetails.member_id}</label></p>
								</div>
								<div class="col-xs-3">
									Request ID: <p><label>${requestId}</label></p>
								</div>
								<div class="col-xs-3">
									Name: <p><label>${PersonalDetails.first_name} ${PersonalDetails.last_name}</label></p>
								</div>
							
								<div class="col-xs-3">
									Gender: <p><label>${PersonalDetails.sex}</label></p>
								</div>
								</div></div>
								<div class="form-group">
							<div class="row" style="margin-left:3px">
								<div class="col-xs-3">
									Age: <p><label>${PersonalDetails.age}</label></p>
								</div>
								<div class="col-xs-3">
									DOB:  <p><label>${PersonalDetails.dob}</label></p>
								</div>
							
								<div class="col-xs-3">
									Insured Status: <p><label>${PersonalDetails.insured}</label></p>
								</div>
								<div class="col-xs-3">
									Payer ID: <p><label>${PersonalDetails.payer_id}</label></p>
								</div>
								</div></div>
								<div class="form-group">
							<div class="row" style="margin-left:3px">
								<div class="col-xs-3">
									Policy ID: <p><label>${PersonalDetails.policy_id}</label></p>
								</div>
							

								<div class="col-xs-3">
									Provider ID: <p><label>${PersonalDetails.provider_id}</label></p>
								</div>
								<div class="col-xs-3">
									Last Treatment Date: <p><label>${PersonalDetails.latest_treatment_date}</label></p>
								</div> 
							</div>
						</div>
						<%-- 						<div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Last Treatment Date:</label><label>${PersonalDetails.latest_treatment_date}</label>
								</div>
								<!-- <div class="col-xs-4">
									<label>Vital Status Source:</label><label>${PersonalDetails.last_reporting_source}</label>
								</div>-->
							</div>
						</div> --%>
						<!-- 						<div class="form-group">
							<button type="submit" class="btn btn-success btn-lg btn-block" >Edit</button>
						</div> -->
						<!-- 	</form> -->
						<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

						<!-- <form action="summarydrugPaRequest" method="post"> -->

						
						<!-- 					<div class="form-group">
							<button type="submit" class="btn btn-success btn-lg btn-block">Update</button>
						</div> -->


						<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->


						<div class="w3-button w3-block"
						style="background-color: #76a0b7; color: #ffffff; height: 40px;">
						<p style="padding-bottom: 6px;margin-top:-3px; font-size:20px">Provider Information</p>
					</div><br>
											<div class="form-group">
							<div class="form-group">
							<div class="row" style="padding: 20px">

								<%-- <div class="col-xs-4">
									<label>Request ID : </label><label>
										${DrugDetails.drug_substitute}</label>
								</div> --%>
								<div class="col-xs-3">
									Provider ID: <p><label>${Provider.provider_ID}</label></p>
								</div>
								<div class="col-xs-3">
									Provider Type: <p><label>${Provider.provider_type}</label></p>
								</div>
								<div class="col-xs-3">
									Speciality: <p><label>${Provider.speciality}</label></p>
								</div>
							<div class="col-xs-3">
									Name: <p><label>${Provider.provider_Name}</label></p>
								</div>
								</div></div>
								<div class="form-group">
							<div class="row" style="margin-left:3px">
								<div class="col-xs-3">
									Contact Number: <p><label>${Provider.phone_no}</label></p>
								</div>
								
								<div class="col-xs-3">
									Address: <p><label>${Provider.address}</label></p>
								</div>
							</div>
						</div>
						<div class="w3-button w3-block"
						style="background-color: #76a0b7; color: #ffffff; height: 40px;">
						<p style="padding-bottom: 6px; margin-top:-3px;  font-size:20px">Medication Details</p>
					</div>
						<br>
						<div class="form-group">
							<div class="row" style="padding: 20px">
								<%-- <div class="col-xs-4">
									<label>Request ID:</label><label>${DrugDetails.request_id}</label>
								</div> --%>
								<div class="col-xs-3">
									Dignosis: <p><label>${DrugDetails.disease}</label></p>
								</div>
								<div class="col-xs-3">
									Medication ID: <p><label>${DrugDetails.drug_id}</label></p>
								</div>
								<div class="col-xs-3">
									Medication Name: <p><label>${DrugDetails.drug_name}</label></p>
								</div>
							
								<div class="col-xs-3">
									Substitute: <p><label>${DrugDetails.drug_substitute}</label></p>
								</div>
								</div></div>
								<div class="form-group">
							<div class="row" style="margin-left:3px">
								<div class="col-xs-3" >
									Max Dosage: <p><label>${DrugDetails.max_dosage}</label></p>
								</div>
								<%-- <div class="col-xs-4">
									<label>Frequency : </label><label>
										${DrugDetails.allow_combination}</label>
								</div> --%>

							</div>
						</div>
						
						<!-- <div class="w3-button w3-block"
						style="background-color: #76a0b7; color: #ffffff; height: 40px;">
						<p style="padding-bottom: 6px; margin-top:-3px;font-size:20px">Lab Reports</p>
					</div><br> -->
						<%-- <div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Blood Pressure:</label><label>&nbsp${LabDetails.blood_pressure}</label>
								</div>
								<div class="col-xs-4">
									<label>Diastolic:</label><label>&nbsp${LabDetails.diastolic}</label>
								</div>
								<div class="col-xs-4">
									<label>Systolic:</label><label>&nbsp${LabDetails.systolic}</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Haemoglobin:</label><label>&nbsp${LabDetails.heamoglobin}</label>
								</div>
								<div class="col-xs-4">
									<label>WBC:</label><label>&nbsp${LabDetails.wbc}</label>
								</div>
								<div class="col-xs-4">
									<label>RBC:</label><label>&nbsp${LabDetails.rbc}</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Blood Sugar Fasting:</label><label>&nbsp${LabDetails.blood_sugar_fasting}</label>
								</div>
								<div class="col-xs-4">
									<label>HIV Status:</label><label>${LabDetails.hiv_status}</label>
								</div>
							</div>
						</div> --%>
						
					</form>

						<div class="form-group" align="center">
							<div class="row" align="center">
								<div class="col-xs-6">
									<!-- <form action="summaryPostPaRequest" method="get"> -->
									<!-- <div class="form-group" > -->

									<a href="summaryPostPaRequest"><button type="button"
											class="btn w3-button " style="background-color: #1cacdc;">
											<font color="#ffffff">Edit</font>
										</button></a>

									<!-- 	</div> -->
									<!-- </form> -->
								</div>

								<div class="col-xs-6">

									<!-- <form action="ViewPaRequest"> -->
									<!-- <div class="form-group" align="center"> -->

									<a href="ViewPaRequest?from=submittedForm"><button type="button"
											class="btn w3-button " style="background-color: #1cacdc;">
											<font color="#ffffff">Submit</font>
										</button></a>

									<!-- </div> -->

									<!-- </form> -->
								</div>
							</div>
						</div>

					</form>


				</div>

				<!-- End Middle Column -->

				<!-- Start Right Column -->
				<div class="w3-col m2">
					<!-- <div class="w3-card w3-round w3-white w3-center">
					<div class="w3-container">
						<br>
						<p>Powered By</p>
						<img src="/images/atos-syntel1.png"
							style="height: 50px; width: 150px"> <br> <br>
					</div>
				</div> -->
					<br>
					<!-- <div class="w3-card w3-round w3-white w3-padding-16 w3-center">
					<p>EIS Practice Demo</p>
				</div> -->
					<br>
					<!-- <div class="w3-card w3-round w3-white w3-padding-32 w3-center">
					<h4>Contact Insurer</h4>
					<p>
						<i class="fa fa-phone" style="font-size: 36px"></i>
					</p>
					<h4>+1-987-654-321</h4>
				</div> -->
				</div>
				<!-- End Right Column -->
			</div>
			<!-- End Grid -->

			<!-- End Page Container -->

			<!-- Footer -->
			<!-- <div class="footer" style="background-color: orange; color: blue">
			<footer class="w3-container" style="background-color: #ededed;">
				
				<p><h6><center>
					PreAuth Powered by <a href="https://www.atos-syntel.net/" target="_blank">AtoS|Syntel
					</a></center></h6>
				</p>
			</footer>
			End Footer
		</div> -->
			<%@ include file="footer.jsp"%>
		</div>

		<script>
			// Accordion
			function myFunction(id) {
				var x = document.getElementById(id);
				if (x.className.indexOf("w3-show") == -1) {
					x.className += " w3-show";
					x.previousElementSibling.className += " w3-theme-d1";
				} else {
					x.className = x.className.replace("w3-show", "");
					x.previousElementSibling.className = x.previousElementSibling.className
							.replace(" w3-theme-d1", "");
				}
			}

			// Used to toggle the menu on smaller screens when clicking on the menu button
			function openNav() {
				var x = document.getElementById("navDemo");
				if (x.className.indexOf("w3-show") == -1) {
					x.className += " w3-show";
				} else {
					x.className = x.className.replace(" w3-show", "");
				}
			}

			//Back with Delete
		</script>
</body>
</html>
