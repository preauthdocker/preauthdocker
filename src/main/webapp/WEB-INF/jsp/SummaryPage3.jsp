<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>Summary Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>
html, body, h1, h2, h3, h4, h5 {
	font-family: "Open Sans", sans-serif
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
}

/* body {
	font-family: 'Roboto', sans-serif;
}
 */

/*Script for Collapse  */
body {
	padding: 50px;
	background: #000;
}

.wrapper {
	width: 70%;
	/* height: 100%; */
}

@media ( max-width :1992px) {
	.wrapper {
		width: 100%;
	}
}

.panel-heading {
	padding: 0;
	border: 0;
	text-color: black;
}

.panel-title>a, .panel-title>a:active {
	display: block;
	padding: 10px;
	color: #555;
	font-size: 16px;
	font-weight: bold;
	text-color: black;
	letter-spacing: 1px;
	word-spacing: 3px;
	text-decoration: none;
}

.panel-heading  a:before {
	font-family: 'Glyphicons Halflings';
	content: "\e114";
	float: right;
	color: silver;
	transition: all 0.5s;
}

.panel-heading.active a:before {
	-webkit-transform: rotate(180deg);
	-moz-transform: rotate(180deg);
	transform: rotate(180deg);
}

/* End  Script for Collapse  */
.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}

.form-control:focus {
	border-color: #;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 1025px;
	height: 785px;
	margin-left: 15px;
	padding: 0px 0;
}

.signup-form h3 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form h3:before, .signup-form h3:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}

.signup-form h3:before {
	left: 0;
}

.signup-form h3:after {
	right: 0;
}

.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}

.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 20px;
}

.signup-form form1 {
	color: #999;
	border-radius: 3px;
	margin-bottom: 0px;
	height: 10px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 0px;
}

/* .signup-form .form-group {
	margin-bottom: 10px;
} */
.signup-form input[type="checkbox"] {
	margin-top: 3px;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
	min-width: 100px;
	max-width: 200px;
	outline: none !important;
}

.signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: gray text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
}

.footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: white;
}

.login-form .form-control {
	background: #f7f7f7 none repeat scroll 0 0;
	border: 1px solid #d4d4d4;
	border-radius: 4px;
	font-size: 14px;
	height: 50px;
	line-height: 50px;
}

#headerTab2 {
	background-color: #006398;
}
</style>

<body class="w3-theme-l5" id="LoginForm">
	<!-- Navbar -->
	<div class="w3-top" style="background-color: #ededed;">
		<div id="headerTab2" style="height: 85px">
			<%--  <a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
				href="javascript:void(0);" onclick="openNav()"><i
				class="fa fa-bars"></i></a> <a href="returnHome"
				class="w3-bar-item w3-button w3-padding-large w3-theme-d4"><i
				class="fa fa-home w3-margin-right"></i>PreAuth</a> 
				<!-- <a href=""
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="Mails"><i class="fa fa-envelope"></i></a> -->
			<div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" title="Notifications">
					<i class="fa fa-bell"></i><span
						class="w3-badge w3-right w3-small w3-green">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
		
				 <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				title="logout"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a href="#"
				class=" w3-right w3-bar-item w3-button w3-padding-large w3-theme-d4"> <i
				class="fa w3-margin-right"></i>Welcome ${Provider.provider_Name}
			</a>   --%>
			<%@ include file="header.jsp"%>
		</div>
	</div>


	<!-- Page Container -->

	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">
		<!-- The Grid -->
		<div class="w3-col m2"
			style="float: right; margin-top: 30px; margin-bottom: 20px">

			<img src="/images/icons/call-icon.png"
				style="float: left; padding-top: 10px" />
			<h6 style="float: right; padding-right: 35px; font-style: Roboto">
				Contact Insurer<br>+1-987-654-3210
			</h6>
		</div>
		<div class="w3-row">
			<!-- Middle Column -->
			<br>
			<div class="w3-col m8" style="margin-top: -30px">
				<div class="signup-form" style="width: 145%; margin-left: 15px">
					<!-- <a href="returnHome" style="float:left; padding-left:1px; margin-top:-93px; font-size:15px;"><font color="#0000ff">Back to Home</font></a> -->
					<!-- Collapsable Starts >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
					<div class="wrapper center-block">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading active" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a role="button" data-toggle="collapse"
											style="text-decoration: none; background-color: #76a0b7;"
											data-parent="#accordion" href="#collapseOne"
											aria-expanded="true" aria-controls="collapseOne"> <font
											color="#ffffff"> Personal Details</font>
										</a>
									</h4>
								</div>

								<div id="collapseOne" class="panel-collapse collapse in"
									role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body">
										<!-- Start Personal Details -->
										<form action="summaryPostPaRequest2" method="get">

											<div class="form-group">
												<div class="row">
													<div class="col-xs-3">
														Request ID: <p><label>
															${requestId}</label></p>
													</div>

													<div class="col-xs-3">
														Member ID: <p><label>
															${PersonalDetails.member_id}</label></p>
													</div>
													<div class="col-xs-3">
														Name: <p><label>
															${PersonalDetails.first_name}
															${PersonalDetails.last_name}</label></p>
													</div>
												<!-- </div>
											</div>
											<div class="form-group">
												<div class="row"> -->
													<div class="col-xs-3">
														Gender: <p><label>${PersonalDetails.sex}</label></p>
													</div>
													<div class="col-xs-3">
														Age: <p><label>${PersonalDetails.age}</label></p>
													</div>
													<div class="col-xs-3">
														&nbspDOB: <p><label> <%-- <input readonly type="date" 
														value=" --%>&nbsp${PersonalDetails.dob}<!-- "/> --></label></p>
													</div>
												</div>
											</div>
										</form>


									</div>
								</div>
							</div>
							<!--  End Personal Details-->
							<!--Provider Information -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											style="text-decoration: none; background-color: #76a0b7;"
											data-parent="#accordion" href="#collapseTwo"
											aria-expanded="false" aria-controls="collapseTwo"> <font
											color="#ffffff">Provider Information</font>
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body">
										<!-- Start Drug Details-->
										<form action="summarydrugPaRequest" method="post">

											<div class="form-group">
												<div class="row">
													<div class="col-xs-6">
														<!-- <label>Request ID:</label> -->
														<input type="number" hidden class="form-control"
															name="request_id" value="${DrugDetails.request_id}"
															required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-6">
														<label>Provider ID:</label><input type="text"
															class="form-control" value="${Provider.provider_ID}"
															name="Provider ID" placeholder="Provider ID" required="required"
															id="">
													</div>
													<div class="col-xs-6">
														<label>First Name:</label><input type="text"
															class="form-control" value="${Provider.provider_Name}"
															name="First_Name" placeholder="First Name"
															required="required" id="">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-6">
														<label>Last Name:</label><input type="text"
															class="form-control" name="Last_Name"
															placeholder="Last Name"
															value="${Provider.last_name}" required="required"
															id="">
													</div>
													<div class="col-xs-6">
														<label>Provider Type:</label><input type="text"
															value="${Provider.provider_type}"
															class="form-control" name="Provider_Type"
															placeholder="Provider Type" required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">

													<div class="col-xs-6">
														<label>Speciality:</label><input type="text"
															value="${Provider.speciality}" class="form-control"
															name="Speciality" placeholder="Speciality"
															required="required" id="">
													</div>
													<div class="col-xs-6">
														<label>Contact Number:</label> <input type="text"
															value="${Provider.phone_no}"
															class="form-control" placeholder="Contact Number"
															name="Contact Number" required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">

													<div class="col-xs-6">
														<label>Address:</label><input type="text"
															value="${Provider.address}" class="form-control"
															name="Address" placeholder="Address"
															required="required" >
													</div>
													<div class="col-xs-6">
														<label>Zipcode:</label> <input type="text"
															value="${Provider.zipcode}"
															class="form-control" placeholder="Zipcode"
															name="Zipcode" required="required">
													</div>
												</div>
											</div>

											<div class="col-xs-5" align="center">
												<label>Comments:</label> <label>${CommentDetails}</label>
											</div>
											<div class="form-group" align="center">
												<textarea rows="4" cols="88" id="comments_disable"
													style="padding-right: 98px" name="comment" align="center"<%-- value="${DrugDetails.drug_comments}" --%>></textarea>
												</br>
												<button type="submit" class="btn w3-button"
													style="background-color: #1cacdc;">
													<font color="#ffffff">Update</font>
												</button>
											</div>
										</form>

									</div>
								</div>
							</div>
							<!--  End Provider Information-->

							<!-- End of Provider Information -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											style="text-decoration: none; background-color: #76a0b7;"
											data-parent="#accordion" href="#collapseThree"
											aria-expanded="false" aria-controls="collapseThree"> <font
											color="#ffffff">Medication Details</font>
										</a>
									</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										<!-- Start Drug Details-->
										<form action="summarydrugPaRequest" method="post">

											<div class="form-group">
												<div class="row">
													<div class="col-xs-6">
														<!-- <label>Request ID:</label> -->
														<input type="number" hidden class="form-control"
															name="request_id" value="${DrugDetails.request_id}"
															required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-6">
														<label>Dignosis:</label><input type="text"
															class="form-control" value="${DrugDetails.disease}"
															name="disease" placeholder="Dignosis" required="required"
															id="disease">
													</div>
													<div class="col-xs-6">
														<label>Medication ID:</label><input type="text"
															class="form-control" value="${DrugDetails.drug_id}"
															name="drug_id" placeholder="Medication ID"
															required="required" id="drugId">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<div class="col-xs-6">
														<label>Medication Name:</label><input type="text"
															class="form-control" name="drug_name"
															placeholder="Medication Name"
															value="${DrugDetails.drug_name}" required="required"
															id="drugName">
													</div>
													<div class="col-xs-6">
														<label>Medication Substitute:</label><input type="text"
															value="${DrugDetails.drug_substitute}"
															class="form-control" name="drug_substitute"
															placeholder="Medication Substitute" required="required">
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">

													<div class="col-xs-6">
														<label>Max Dosage:</label><input type="text"
															value="${DrugDetails.max_dosage}" class="form-control"
															name="max_dosage" placeholder="Max Dosage"
															required="required" id="maxDosage">
													</div>
													<div class="col-xs-6">
														<label>Allow Combinations:</label> <input type="text"
															value="${DrugDetails.allow_combination}"
															class="form-control" placeholder="Allowed Combination"
															name="allow_combination" required="required">
													</div>
												</div>
											</div>

											<div class="col-xs-5" align="center">
												<label>Comments:</label> <label>${CommentDetails}</label>
											</div>
											<div class="form-group" align="center">
												<textarea rows="4" cols="88" id="comments_disable"
													style="padding-right: 98px" name="comment" align="center"<%-- value="${DrugDetails.drug_comments}" --%>></textarea>
												</br>
												<button type="submit" class="btn w3-button"
													style="background-color: #1cacdc;">
													<font color="#ffffff">Update</font>
												</button>
											</div>
										</form>

									</div>
								</div>
							</div>
							<!-- End Drug Details -->

							<%-- <div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											style="text-decoration: none; background-color: #76a0b7;" data-parent="#accordion"
											href="#collapseThree" aria-expanded="false" 
											aria-controls="collapseThree"> <font color="#ffffff">
												Lab Reports</font>
										</a>
									</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">


										<form action="summaryLabForm" method="post">
											
											<div class="form-group">
												<div class="row" hidden="true">
													<div class="col-xs-6">
														<!-- <label>Request ID:</label> -->
														<input type="number" hidden class="form-control"
															name="request_id" value="${LabDetails.request_id}"
															required="required">
													</div>
													<div class="col-xs-6">
														<label>Member ID:</label><input type="number"
															class="form-control" name="member_id"
															value="${jsonData.memberId}" required="required">
													</div>
												</div>

												<div class="form-group">
													<div class="row">
														<div class="col-xs-6">
															<label>Blood Pressure:</label><input type="text"
																value="${LabDetails.blood_pressure}"
																class="form-control" name="blood_pressure"
																placeholder="Blood Pressure" required="required" id="BP">
														</div>
														<div class="col-xs-6">
															<label>Diastolic:</label><input type="text"
																value="${LabDetails.diastolic}" class="form-control"
																name="diastolic" placeholder="Diastolic"
																required="required" id="Diastolic">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-xs-6">
															<label>Systolic:</label><input type="text" id="Systolic"
																value="${LabDetails.systolic}" class="form-control"
																name="systolic" placeholder="Systolic"
																required="required">
														</div>
														<div class="col-xs-6">
															<label>Haemoglobin:</label><input type="text" id="Haemo"
																value="${LabDetails.heamoglobin}" class="form-control"
																name="heamoglobin" placeholder="Haemoglobin"
																required="required">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-xs-4">
															<label>WBC:</label><input type="text" id="wbc"
																value="${LabDetails.wbc}" class="form-control"
																name="wbc" placeholder="WBC" required="required">
														</div>
														<div class="col-xs-4">
															<label>RBC:</label><input type="text" id="rbc"
																value="${LabDetails.rbc}" class="form-control"
																name="rbc" placeholder="RBC" required="required">
														</div>
														<div class="col-xs-4">
															<label>Blood Sugar Level:</label><input type="text"
																id="BSF" value="${LabDetails.blood_sugar_fasting}"
																class="form-control" name="blood_sugar_fasting"
																placeholder="Blood Sugar Fasting" required="required">
														</div>
													</div>
												</div>
																								<div class="form-group">
													<div class="row">

														<div class="col-xs-6">
															<label>HIV Status:</label><input type="text"
																value="${LabDetails.hiv_status}" class="form-control"
																id="HIV" name="hiv_status" placeholder="Hiv Status"
																required="required">
														</div>
													</div>
												</div>
												<div class="form-group" align="center">
													<!-- <a class="trigger_popup_fricc"> -->
													<button type="submit" class="btn w3-button" style="background-color: #1cacdc;"><font color="#ffffff">Update</font></button>
													<!-- </a> -->
												</div>
										</form>

									</div>
								</div>
							</div> --%>
							<!-- Comment Section  -->
							<%-- <div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingFour">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											style="text-decoration: none" data-parent="#accordion"
											href="#collapseFour" aria-expanded="false"
											aria-controls="collapseFour"> <font color="#636363">
												Comments </font>
										</a>
									</h4>
								</div>
								<div id="collapseFour" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingFour">
									<div class="panel-body" align="center">
									
										<form action="summaryComment" method="post">
											<!--  -->
											<input type="number" hidden class="form-control"
															name="request_id" value="${DrugDetails.request_id}">
											<label>Comments :</label> <br>

									x		<textarea rows="4" cols="75" id="comments_disable"
												name="comment"></textarea>
											<!--  -->

											<div class="form-group" align="center">
												<a class="trigger_popup_fricc">
													<button type="submit"
														class="btn btn-success">Submit
														Comment</button>
												</a>
											</div>
											</form>
									</div>
								</div>
							</div> --%>
							<!-- Comment Section Ends -->
						</div>
					</div>


						<FORM align="center" id="signup-form form ">
						<div class="form-group" align="center">

							</br>
							<div class="row" align="center">
								<div class="col-xs-6">
									<!-- <FORM align="center" id="backForm"> -->
									<INPUT TYPE="button" class="btn w3-button"
										style="background-color: #1cacdc; color: #ffffff" VALUE="Back"
										onClick="backWithDelete()">
									<!-- </FORM> -->
								</div>

								<div class="col-xs-6">

									<!-- <form action="ViewPaRequest"> -->
									<!-- <a class="trigger_popup_fricc" style="text-decoration: none"> -->
									<a href="ViewPaRequest?from=submittedForm"><button type="button"
											class="btn w3-button" style="background-color: #1cacdc;">
											<font color="#ffffff">Submit</font>
										</button></a>
									<!-- </a> -->
									<!-- 	</form> -->
								</div>
							</div>
						</div>
					</FORM>
				</div>
			</div>
		</div>
		<!-- End Middle Column -->

	</div>
	<!-- End Grid -->

	<!-- End Page Container -->

	<%@ include file="footer.jsp"%>
	<!-- Footer -->
	<!-- <div class="footer" style="background-color: orange; color: blue">
		<footer class="w3-container w3-theme-d3">
			<h5>PreAuth</h5>
			<p>
				Powered by <a href="https://www.atos-syntel.net/" target="_blank">AtoS|Syntel
				</a>
			</p>
		</footer>
	</div>  -->

	<!-- End Footer -->

	</div>
	<script>
		//Back with Delete
		function backWithDelete() {
			//document.getElementById("backForm").submit();
			history.go(-1);

		}

		// Accordion
		function myFunction(id) {
			var x = document.getElementById(id);
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
				x.previousElementSibling.className += " w3-theme-d1";
			} else {
				x.className = x.className.replace("w3-show", "");
				x.previousElementSibling.className = x.previousElementSibling.className
						.replace(" w3-theme-d1", "");
			}
		}

		// Used to toggle the menu on smaller screens when clicking on the menu button
		function openNav() {
			var x = document.getElementById("navDemo");
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
			} else {
				x.className = x.className.replace(" w3-show", "");
			}
		}

		/* Start Script for Collapse */
		$('.panel-collapse').on('show.bs.collapse', function() {
			$(this).siblings('.panel-heading').addClass('active');
		});

		$('.panel-collapse').on('hide.bs.collapse', function() {
			$(this).siblings('.panel-heading').removeClass('active');
		});

		/* End Script for Collapse */
	</script>
</body>
</html>
