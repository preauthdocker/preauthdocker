<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
<title>Registration Form</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style type="text/css">
	body{
	background-image:
		/* url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
		color: #ededed;
		
		font-family: 'Roboto', sans-serif;
	}
    .form-control{
		height: 40px;
		box-shadow: none;
		color: #969fa4;
	}
	.form-control:focus{
		border-color: #5cb85c;
	}
    .form-control, .btn{        
        border-radius: 3px;
    }
	.signup-form{
		width: 500px;
		margin: 0 auto;
		padding: 30px 0;
	}
	.signup-form h2{
		color: #636363;
        margin: 0 0 15px;
		position: relative;
		text-align: center;
    }
	.signup-form h2:before, .signup-form h2:after{
		content: "";
		height: 2px;
		width: 30%;
		background: #d4d4d4;
		position: absolute;
		top: 50%;
		z-index: 2;
	}	
	.signup-form h2:before{
		left: 0;
	}
	.signup-form h2:after{
		right: 0;
	}
    .signup-form .hint-text{
		color: #999;
		margin-bottom: 30px;
		text-align: center;
	}
    .signup-form form{
		color: #999;
		border-radius: 3px;
    	margin-bottom: 15px;
        background: #f2f3f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
	.signup-form .form-group{
		margin-bottom: 20px;
	}
	.signup-form input[type="checkbox"]{
		margin-top: 3px;
	}
	.signup-form .btn{        
        font-size: 16px;
        font-weight: bold;		
		min-width: 140px;
        outline: none !important;
    }
	.signup-form .row div:first-child{
		padding-right: 10px;
	}
	.signup-form .row div:last-child{
		padding-left: 10px;
	}    	
    .signup-form a{
		color: #fff;
		text-decoration: underline;
	}
    .signup-form a:hover{
		text-decoration: none;
	}
	.signup-form form a{
		color: #5cb85c;
		text-decoration: none;
	}	
	.signup-form form a:hover{
		text-decoration: underline;
	} 
	#headerTab2{
background-color:#006398;
} 
</style>
</head>




<body>

<div class="signup-form">
    <form action="/examples/actions/confirmation.php" method="post">
		<h2>Register</h2>
		<p class="hint-text"> Create your account. It's free and only takes a minute.</p>
       <div>
         <div class="form-group">
        	<input type="number" class="form-control" name="member_id" placeholder="MemberId" required="required">
        </div>
			
			<div class="form-group">
			<div class="row"> 	
				<div class="col-xs-6"><input type="text" class="form-control" name="first_name" placeholder="First Name" required="required"></div>
				<div class="col-xs-6"><input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required"></div>
			    </div> 
			    </div>  	
        </div>
       <div class="row"> 	
				
		<div class="form-group">
    		<div class="col-sm-6">
        <label for="input-type">Gender:</label>
        <div id="input-type" class="row">
            <div class="col-sm-6">
                <label class="radio-inline">
                    <input name="sex" id="input-type-male" value="Male" type="radio" />Male
                </label>
            </div>
            <div class="col-sm-6">
                <label class="radio-inline">
                    <input name="sex" id="input-type-female" value="Female" type="radio" />Female
                </label>
            </div>
        </div>
    </div>
</div>	
        </div>
        <div class="form-group">
        	<input type="email" class="form-control" name="age" placeholder="Email" required="required">
        </div>
		<div class="form-group">
            <input type="password" class="form-control" name="dob" placeholder="Password" required="required">
        </div>
		<div class="form-group">
            <input type="password" class="form-control" name="insured" placeholder="Confirm Password" required="required">
        </div>        
        <!-- <div class="form-group">
			<label class="checkbox-inline"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
		</div> -->
		<div class="form-group">
        	<input type="text" class="form-control" name="payer_id" placeholder="payer_id" required="required">
        </div>
        <div class="form-group">
        	<input type="text" class="form-control" name="policy_id" placeholder="policy_id" required="required">
        </div>
        <div class="form-group">
        	<input type="text" class="form-control" name="latest_treatment_date" placeholder="latest_treatment_date" required="required">
        </div>
         <div class="form-group">
        	<input type="text" class="form-control" name="vital_status" placeholder="vital_status" required="required">
        </div>
        <div class="form-group">
        	<input type="text" class="form-control" name="last_reporting_source" placeholder="last_reporting_source" required="required">
        </div>
        <div class="form-group">
        	<input type="text" class="form-control" name="provider_id" placeholder="provider_id" required="required">
        </div>
        <div class="form-group">
        	<input type="text" class="form-control" name="request_status" placeholder="request_status" required="required">
        </div>
        
		<div class="form-group">
            <button type="submit" class="btn btn-success btn-lg btn-block">Submit Data</button>
        </div>
    </form>
	<div class="text-center">Already have an account? <a href="#">Sign in</a></div>
</div>

</body>
</html>