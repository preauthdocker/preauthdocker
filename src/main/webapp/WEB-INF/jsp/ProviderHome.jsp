<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<title>ProviderPortalHomePage</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

<style>
.dataTables_filter, .dataTables_info, .dataTables_LengthChange,
	.dataTables_AutoWidth {
	display: none;
}

html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif;
}

#Details {
	/* font-family: "Trebuchet MS", Arial, Helvetica, sans-serif; */
	font-family: "Roboto", Helvetica, sans-serif;
	border-collapse: collapse;
	width: 100%;
	
}

#Details td, #Details th {
	border: 1px solid #ddd;
	padding: 8px;
	
}

body, html {
	height: 100%;
	margin: 0;
}

#Details tr:nth-child(even) {
	background-color: #f2f2f2;
	
}

#Details tr:hover {
	background-color: #ddd;
	
}

#searchError {
	color: red;
}

#Details th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: center;
	background-color: #6e8d9e;
	color: white;
	
}

tr, th {
	padding: 10px;
	text-align: center;
}

#headerTab2 {
	background-color: #006398;
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
	height: 100%;
}
/* .w3-card{box-shadow:0 2px 5px 0 rgba(0,0,255,0.7),0 2px 10px 0 rgba(0,0,0,0.12) */
.footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}

.signup-form {
	width: 1040px;
	margin: 0 auto;
	padding: 22px 0;
}

.signup-form h2 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form h2:before, .signup-form h2:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}

.signup-form h2:before {
	left: 0;
}

.signup-form h2:after {
	right: 0;
}

.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}

/* .signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}
 */
.signup-form .form-group {
	margin-bottom: 20px;
}

.signup-form input[type="checkbox"] {
	margin-top: 3px;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
	min-width: 140px;
	max-width: 200px;
	outline: none !important;
}

.signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

#WelcomeProvider {
	align: 10px;
}

.signup-form a {
	color: #006398;
	 /* text-decoration: underline;  */
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}

.signup-form form a:hover {
	  text-decoration: underline;  
}

.login-form .form-control {
	background: #f7f7f7 none repeat scroll 0 0;
	border: 1px solid #d4d4d4;
	border-radius: 4px;
	font-size: 14px;
	height: 50px;
	line-height: 50px;
}

#headerTab2 {
	background-color: #006398;
}

#font {
	font-style: Roboto;
}
#SR{
background-image:url('/images/icons/SearchIcon.png');
position:left;
padding-left:30px;
background-size:25px;
background-repeat:no-repeat;

/* border-radius:8px; */
}

</style>
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script
	src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
	function getMemberDetails() {
		/*  $.ajax({
			
			type:"GET",
		    url: "http://localhost:5006/preauth/memberUniData/"+
		}).then(function(data) 
		{ */
		data = document.getElementById("searchIdToTake").innerHTML;

		if (data == "") {
			$('#tableToHide').hide();
		} else {
			$('#Details').DataTable({
				"language" : {

					"lengthMenu" : "Display _MENU_ records",
					"paginate" : {
						'previous' : '&lt',
						'next' : '&gt'
					}
				},

				"info" : false,
				"bLengthChange" : false

			});

			$('#tableToHide').show();
		}
		// });
	}

	function validate() {

		var memberId = document.getElementById("memberId").value;
		if (memberId.length < 3) {
			document.getElementById("searchError").innerHTML = "Please Enter more than 3 characters";
			//alert(document.getElementById("searchError").innerHTML);
			//alert("memberID"+memberId);
			return false
		}
	}

	$(document).ready(function() {
		getMemberDetails();

	});
</script>
</head>

<body class="w3-theme-l5" id="LoginForm">
	<!-- Navbar -->

	<div class="w3-top" >
		<!-- <div class="w3-bar w3-theme-d2 w3-left-align w3-large"> -->
		
			<%--  <a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" 
				href="javascript:void(0);" onclick="openNav()"><i
				class="fa fa-bars"></i></a> <a href="returnHome" style= "color: #ffffff;"
				><i
				class="w3-margin-right" ></i><b>PreAuth</b></a> 
				<!-- <a href="#"
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="Mails"><i class="fa fa-envelope"></i></a> --><br>
			<div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" title="Notifications">
					<i class="bell fa fa-bell-o"></i><span
						class="w3-badge w3-right w3-small w3-orange">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
			
				<!-- <a
				href="#"
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="News"><i class="fa fa-newspaper-o"></i></a> -->
				 <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				title="Logout"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a id="WelcomeProvider" href="#"
				class=" w3-right" style= "color: #ffffff;"><i
				class="fa w3-margin-right "></i>Welcome ${Provider.provider_Name}</a> 
				
				<!-- logout dropdown 
				<div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" >

				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">Logout
			</a>
				</div>
			</div>
				
				
				
				--> 
				 --%>
			<%--  <div id="Row1" style="height:15px"></div>
				<div  id="rr" class="row">
            <div  class="column w3-padding-large"  style= "color: #ffffff; padding-top:25px">
            <h3 style="padding-left:30px;"><b>PreAuth</b></h3>
            </div>
	        <div  class="column" style="padding-left:850px;">
            <div class="  w3-hide-small w3-right ">
				<button class="w3-button w3-padding-large " title="Notifications">
					<i class="bell fa fa-bell-o w3-xxlarge" style="color:#ffffff;"></i><span
						class="w3-badge w3-right w3-small w3-orange">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
			
			 <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-none"
				title="Logout"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a id="WelcomeProvider" 
				class=" w3-right" style= "color: #ffffff;"><i
				class="fa w3-margin-right "></i>Welcome<br>${Provider.provider_Name}</a> 
          </div>
	      </div>
	      	<div id="Row3"></div> --%>
			<%@ include file="header.jsp"%>
		</div>
	
	<!-- Column
	<div class="row">
  <div class="column" style= "color: #ffffff;">
    <h5>PreAuth</h5>
  </div>
	<div class="column">
    <div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" title="Notifications">
					<i class="bell fa fa-bell-o"></i><span
						class="w3-badge w3-right w3-small w3-orange">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
			
			 <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				title="Logout"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a id="WelcomeProvider" href="#"
				class=" w3-right" style= "color: #ffffff;"><i
				class="fa w3-margin-right "></i>Welcome ${Provider.provider_Name}</a> 
  </div>
	</div>
	
	
	 -->



	<!-- Navbar on small screens -->


	<!-- Page Container -->

	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">
		<!-- The Grid -->

		<div class="w3-row">
			<!-- Middle Column -->
			<div class="w3-col m8"  >
				<div class="signup-form">
					<div class="w3-row-padding" >
						<div class="w3-col m12" >
							<!-- <div class="w3-card w3-round w3-white"> -->
							<!-- <div class="w3-container w3-padding" > -->
								<form action="MemberDetails" >
									<p>
									<p>
									<h6 style="font-style: Roboto; font-size: 18px;">Search
										Member</h6>

									</p>
									</p>
									<!-- <h6 class="w3-opacity"></h6> -->
									<div  class="ps-relative">
									
									<!-- <svg aria-hidden="true" class="svg-icon s-input-icon s-input-icon__search iconSearch" width="18" height="18" viewBox="0 0 18 18"><path d="M18 16.5l-5.14-5.18h-.35a7 7 0 1 0-1.19 1.19v.35L16.5 18l1.5-1.5zM12 7A5 5 0 1 1 2 7a5 5 0 0 1 10 0z"></path></svg>
									<input name="q" type="text" placeholder="Search" value="" autocomplete="off" maxlength="240" class="s-input s-input__search js-search-field " aria-label="Search" aria-controls="top-search" data-controller="s-popover" data-action="focus->s-popover#show" data-s-popover-placement="bottom-start"> -->
									<input  class="w3-border w3-padding w3-opacity"  
										style="width: 555px;" type="text"
										 name="MemberId" placeholder="Search By Id/FirstName/LastName" 
										id="memberId" width=100%>
										
									<button type="submit" class="w3-button "
										style="background-color: #1cacdc; font-size: 16px; font-style: Roboto;"
										onClick="return validate()">
										<font color="#ffffff"><b>Search</b></font>
									</button></div>
									<div id="searchError" align="center"></div>
								</form>
								
							<!-- </div> -->
							<!-- </div> -->
						</div>
					</div>



					<div class="w3-container w3-card w3-white w3-round w3-margin"
						id="tableToHide" style="display: none;width: 115%;">
						<br>
						<div class="w3-col m13" >
							<h6 style="padding-left: 22px; font-size: 22px;">Member List</h6>
							<div class="container" align="center">
								<!-- <table id="Details"> -->

								<table id="Details" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th align="center"
												style="background-color: #76a0b7; font-size: 14px;">Member
												ID</th>
											<th align="center"
												style="background-color: #76a0b7; font-size: 14px;">First
												Name</th>
											<th align="center"
												style="background-color: #76a0b7; font-size: 14px;">Last
												Name</th>
											<th align="center"
												style="background-color: #76a0b7; font-size: 14px;">Gender</th>
											<th align="center"
												style="background-color: #76a0b7; font-size: 14px;">Age</th>
											<th align="center"
												style="background-color: #76a0b7; font-size: 14px;">Create
												PA Request</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="i" items="${jsonData}">
											<tr>
												<td align="center" style="font-size: 14px;"><a href="Eligibilty/${i.memberId}">${i.memberId}</a></td>
												<td align="center" style="font-size: 14px;">${i.firstName}</td>
												<td align="center" style="font-size: 14px;">${i.lastName}</td>
												<td align="center" style="font-size: 14px;">${i.sex}</td>
												<td align="center" style="font-size: 14px;">${i.age}</td>

												<td align="center" style="font-size: 14px;"><a
													href="PostPaRequest/${i.memberId}"><font
														color="#1329fb"> Create PA Request</font></a></td>
											</tr>
										</c:forEach>
									</tbody>

								</table>
								<br>
							</div>
						</div>
					</div>

					<div id="resultDisplay"></div>
				</div>
			</div>
			<!-- Right Column -->
			<div class="w3-col m2" style="padding-left:200px;">
				<br>
				<!-- Profile -->
				<!-- <div class="w3-card w3-round w3-white"> -->

 
					<div class="w3-container" align="center">

						<br>
						<form action="ViewPaRequest">
							<button type=submit id="viewpaButton" class="w3-button " img src="/images/icons/PA_folder.png"  
								style="background-color: #1cacdc; font-style: Roboto; font-size: 15px; width:175px; height:40px;">
								<img src="/images/icons/PA_folder.png" />&nbsp<font color="#ffffff"><b>&nbspView PA Request</b></font>
							</button>
						</form>
						</br> <br>
					</div>
				<!-- </div> -->
			</div>


			<div id="searchIdToTake" style="display: none;">${searchId}</div>

			<!-- End Left Column -->
			<!-- End Middle Column -->


			<br> <br>


			<!-- End Right Column -->
		</div>
		<br> <br>
		<!-- End Grid -->
		<br>
		<!-- End Page Container -->
		<br> <br>

		<!-- Footer -->
		<%@ include file="footer.jsp"%>
	</div>

	<script>
		// Accordion
		function myFunction(id) {
			var x = document.getElementById(id);
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
				x.previousElementSibling.className += " w3-theme-d1";
			} else {
				x.className = x.className.replace("w3-show", "");
				x.previousElementSibling.className = x.previousElementSibling.className
						.replace(" w3-theme-d1", "");
			}
		}

		// Used to toggle the menu on smaller screens when clicking on the menu button
		function openNav() {
			var x = document.getElementById("navDemo");
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
			} else {
				x.className = x.className.replace(" w3-show", "");
			}
		}
	</script>
</body>
</html>
