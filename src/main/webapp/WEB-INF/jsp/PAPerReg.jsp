<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>Registration Form</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif;
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
}

body {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	color: #ededed;
	font-family: 'Roboto', sans-serif;
}

.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}

.form-control:focus {
	border-color: #5cb85c;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 1010px;
	margin-left: 15px;
	padding: 20px 0;
	height: 70%;
}

.signup-form h2 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}

.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-top: 50px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}

.signup-form .form-group {
	margin-bottom: 20px;
}

.signup-form input readonly[type="checkbox"] {
	margin-top: 3px;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
	min-width: 140px;
	max-width: 200px;
	outline: none !important;
}

.signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
}

footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}

input readonly:read-only {
	background-color: #D3D3D3;
}

#headerTab2 {
	background-color: #006398;
}

#WelcomeProvider {
	align: 10px;
}

.contactImageStyle {
	float: right;
	position: relative;
	right: 80px;
	top: 30px;
}

.contactNumberStyle {
	position: relative;
	left: 50px;
	bottom: 35px;
}

 
</style>

<body class="w3-theme-l5" id="LoginForm">


	<!-- Navbar -->
	  
	<div class="w3-top">

		<%@ include file="header.jsp"%>
	</div>


	<!-- Navbar on small screens -->
	<div id="navDemo"
		class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">My
			Profile</a>
	</div>

	<!-- Page Container -->
	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">

		<div class="contactImageStyle">
			<img src="/images/icons/call-icon.png" />
			<div class="contactNumberStyle">
				Contact Insurer <br>+1-987-654-3210
			</div>
		</div>
		<div class="w3-col m8" style="margin-top: 25px">
			<div class="signup-form" style="width: 145%; margin-left: 15px">
				<form action="drugPaRequest" method="post">
					<label value=${requestIdToPass}></label>
					<div class="w3-button w3-block"
						style="background-color: #76a0b7; color: #ffffff; height: 40px;">
						<p style="padding-bottom: 6px; margin-top: -3px; font-size: 20px">Personal
							Details</p>
					</div>
					<br>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6">
								<label>Member ID :</label> <input readonly readonly
									type="number" class="form-control" name="member_id"
									value="${jsonData.memberId}" required="required" readonly>
							</div>
							<div class="col-xs-6">
								<label>Provider ID:</label> <input readonly type="text"
									class="form-control" value="${Provider.provider_ID}"
									name="provider_id" placeholder="provider_id"
									required="required">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6">
								<label>First Name:</label> <input readonly type="text"
									value="${jsonData.firstName}" class="form-control"
									name="first_name" placeholder="First Name" required="required">
							</div>
							<div class="col-xs-6">
								<label>Last Name:</label> <input readonly type="text"
									class="form-control" name="last_name"
									value="${jsonData.lastName}" placeholder="Last Name"
									required="required">
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6">
								<label>Gender:</label> <input readonly type="text"
									value="${jsonData.sex}" class="form-control" name="sex"
									placeholder="Gender" required="required">
							</div>
							<div class="col-xs-6">
								<label>Age:</label> <input readonly type="number"
									value="${jsonData.age}" class="form-control" name="age"
									placeholder="age" required="required">
							</div>
							
						</div>
					</div>
						<div class="form-group">
						<div class="row">
					<div class="col-xs-6">
								<label>DOB:</label> <input readonly type="date"
									value="${jsonData.dob}" class="form-control" name="dob"
									placeholder="dob" required="required">
							</div>
							<div class="col-xs-6">
								<label>Allergies:</label> <input readonly type="text"
									value="${jsonData.Allergies}" class="form-control" name="Allergies"
									placeholder="Sinus" required="required">
							</div>
							</div>
							</div>
					<div class="form-group">
						<div class="w3-button w3-block"
							style="background-color: #76a0b7; color: #ffffff; height: 40px;">
							<p
								style="padding-bottom: 6px; font-size: 20px; margin-top: -3px;">Request
								Details</p>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-6">
								<label>Insured Status:</label> <input readonly type="text"
									class="form-control" value="${jsonData.insuredStatus}"
									name="insured" placeholder="insured" required="required">
							</div>
							<div class="col-xs-6">
								<label>Insurer ID:</label> <input readonly type="text"
									class="form-control" value="${jsonData.payerId}"
									name="payer_id" placeholder="payer_id" required="required">
							</div>
						</div>
					<br>
					<div class="form-group">
						<div class="row">
							<div class="col-xs-6">
								<label>Policy ID:</label> <input readonly type="text"
									class="form-control" value="${jsonData.policyId}"
									name="policy_id" placeholder="policy_id" required="required">
							</div>
							<div class="col-xs-6">
								<label>Latest Treatment Date:</label> <input readonly
									type="date" class="form-control"
									value="${jsonData.latestTreatmentInitiationDate}"
									name="latest_treatment_date"
									placeholder="latest_treatment_date" required="required">
							</div>
						</div>
					</div>
					<div>
						<div class="col-xs-5" align="center">
							<!-- <label>Comments :</label>  -->
							<br>
						</div>
						<br>
						<div class="form-group" align="center">
							<%-- <textarea rows="4" cols="88" id="comments_disable"
										name="comments" align="center" value="${jsonData.comment}"></textarea> --%>
							</br>
							<button type="submit" class="btn w3-button "
								style="background-color: #1cacdc;">
								<font color="#ffffff">Next</font>
							</button>
						</div>
					</div>
					<div class="form-group" align="center"></div>
				</form>

			</div>
		</div>
	</div>

	<%@ include file="footer.jsp"%>
	</div>
	<script>
	


	function callSecondURL()
	{
		//alert("Drug Details Bharlo allowed combination and ek aur");
		$.ajax({
	    	type:"GET",
	    	url: "http://localhost:6001/preauth/memberDetails/prescriptionDetails/"+${jsonData.memberId}
		/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
	   /*      url: "http://localhost:6001/preauth/memberDetails/prescriptionDetails/"+${jsonData.memberId} */
	    }).then(function(data) {
	    	
	    	$('#drugId').val(data.pre_p[0].drug_data[0].drug_id);
	    	$('#drugName').val(data.pre_p[0].drug_data[0].drug_name);
	    	 //$('#result').val(data);
	    	 $('#disease').val(data.pre_p[0].drug_data[0].disease);
	    	$('#maxDosage').val(data.pre_p[0].drug_data[0].max_dosage);
	    	//alert("sajdhskjhd");
	    	  
	    });
		
	}

	$(document).ready(function() {
		   callSecondURL();
		 
		});

	
// Accordion
function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    x.previousElementSibling.className += " w3-theme-d1";
  } else { 
    x.className = x.className.replace("w3-show", "");
    x.previousElementSibling.className = 
    x.previousElementSibling.className.replace(" w3-theme-d1", "");
  }
}

// Used to toggle the menu on smaller screens when clicking on the menu button
function openNav() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
function showPopup()
{

if(document.getElementById("comments_disable").value == "")
	{
	document.getElementById("errorComment").innerHTML="*Comments can't be left blank";
		return false;
	}
else
	{
if(document.getElementById("currentStatus").value != null)
	{
	var confirmButton=confirm("Are you sure you want to continue?");
	
	if(confirmButton==false)
		{
		
		return false;
		}
	}
	}
}	

.login-form .form-control {
	  background: #f7f7f7 none repeat scroll 0 0;
	  border: 1px solid #d4d4d4;
	  border-radius: 4px;
	  font-size: 14px;
	  height: 50px;
	  line-height: 50px;
	}
</script>

</body>
</html>
