<!DOCTYPE html>
<html>
<title>Lab Reports</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>Registration Form</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
}

body {
	background-image:
		/* url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
		color : #ededed;
	font-family: 'Roboto', sans-serif;
}

.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}

.form-control:focus {
	border-color: #5cb85c;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 1010px;
	height: 785px;
	margin-left: 15px;
	padding: 20px 0;
}

.signup-form h2 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form h2:before, .signup-form h2:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}

.signup-form h2:before {
	left: 0;
}

.signup-form h2:after {
	right: 0;
}

.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}

.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}

.signup-form .form-group {
	margin-bottom: 20px;
}

.signup-form input[type="checkbox"] {
	margin-top: 3px;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
	min-width: 100px;
	max-width: 200px;
	outline: none !important;
}

.signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
}
/* Popup box BEGIN */
.hover_bkgr_fricc {
	background: rgba(0, 0, 0, .4);
	cursor: pointer;
	display: none;
	height: 8%;
	position: fixed;
	text-align: center;
	top: 200px;
	width: 40%;
	z-index: 10000;
}

.hover_bkgr_fricc .helper {
	display: inline-block;
	height: 30%;
	vertical-align: middle;
}

.hover_bkgr_fricc>div {
	background-color: #fff;
	box-shadow: 10px 10px 60px #555;
	display: inline-block;
	height: auto;
	max-width: 551px;
	min-height: 50px;
	vertical-align: center;
	width: 60%;
	position: relative;
	border-radius: 8px;
	padding: 15px 5%;
}

.popupCloseButton {
	background-color: #fff;
	border: 3px solid #999;
	border-radius: 50px;
	cursor: pointer;
	display: inline-block;
	font-family: arial;
	font-weight: bold;
	position: absolute;
	top: -20px;
	right: -20px;
	font-size: 25px;
	line-height: 30px;
	width: 30px;
	height: 30px;
	text-align: center;
}

.popupCloseButton:hover {
	background-color: #ccc;
}

.trigger_popup_fricc {
	cursor: pointer;
	font-size: 20px;
	vertical-align: middle;
	margin: 20px;
	display: inline-block;
	font-weight: bold;
}
/* Popup box BEGIN */
footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}

#headerTab2 {
	background-color: #006398;
}
.contactImageStyle {
	float: right;
	position: relative;
	right: 80px;
	top: 30px;
}

.contactNumberStyle {
	position: relative;
	left: 50px;
	bottom: 35px;
}
</style>

<script>
/* function callSecondURL()
{
	//alert("pLEASE FILL IN THE dETAILS lEFT BLANK ");
	$.ajax({
    	type:"GET",
        url: "http://localhost:6003/preauth/memberDetails/labReports/"+${jsonData.memberId}
	/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
/* 	url: "http://localhost:6003/preauth/memberDetails/labReports/"+${jsonData.memberId} */
    }).then(function(data) {
    	
    	$('#BP').val(data.vitalStats.blood_pressure);
    	//alert(data.vitalStats.blood_pressure)
    	//$('#drugName').val(data.vitalStats.blood_pressure);
    	 //$('#result').val(data);
    	$('#Diastolic').val(data.vitalStats.diastolic);
    	
    	$('#BSF').val(data.vitalStats.bloodsugar_fasting);
    	$('#HIV').val(data.vitalStats.hiv_stat);
    	$('#Haemo').val(data.vitalStats.haemoglobin);
    	$('#Systolic').val(data.vitalStats.systolic);
    	//alert("sajdhskjhd");
    });	
} */

$(document).ready(function() {
	   callSecondURL();
	 
	});
	
$(window).load(function () {
     $(".trigger_popup_fricc").click(function()
    		 {
    	 if(document.getElementById("rbc").value==""||document.getElementById("wbc").value=="")
    		 {
    		 return false;
    		 }
    	 // alert("Submitted Successfully");
      // $('.hover_bkgr_fricc').show();
    }); 
    /*$('.hover_bkgr_fricc').click(function(){
        $('.hover_bkgr_fricc').hide();
    });
    $('.popupCloseButton').click(function(){
    	
        $('.hover_bkgr_fricc').hide();
    }); */    
});
/* $( "button" ).click(function() {
	  $( "div.first" ).slideUp( 300 ).delay( 800 ).fadeIn( 400 );
	  });
 */
</script>

<body class="w3-theme-l5" id="LoginForm">


	<!-- Navbar -->
	<div class="w3-top">
		
			<%-- <a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
				href="javascript:void(0);" onclick="openNav()"><i
				class="fa fa-bars"></i></a> <a href="returnHome"
				style="color: #ffffff;"><i
				class="fa fa-home w3-margin-right"></i>PreAuth</a> <!-- <a href="#"
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="Mails"><i class="fa fa-envelope"></i></a> -->
			<div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" title="Notifications">
					<i class="fa fa-bell"></i><span
						class="w3-badge w3-right w3-small w3-green">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
<!-- 			 <a
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="News"><i class="fa fa-newspaper-o"></i></a> --> <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				title="logout"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a href="#"
				class=" w3-right" style="color: #ffffff;"> <i
				class="fa w3-margin-right"></i>Welcome ${Provider.provider_Name}
			</a> --%>
			<%@ include file="header.jsp"%>
		
	</div>

	<!-- Navbar on small screens -->
	<div id="navDemo"
		class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">My
			Profile</a>
	</div>

	<!-- Page Container -->
	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">
		<!-- The Grid -->
		<div class="contactImageStyle">
			<img src="/images/icons/call-icon.png" />
			<div class="contactNumberStyle">
				Contact Insurer <br>+1-987-654-321
			</div>
		</div>
		<div class="w3-row">
			<!-- Middle Column -->
			<div class="w3-col m8" style="margin-top:-8px">
				<div class="signup-form" style="width: 145%; margin-left: 15px">
				<!-- <a href="returnHome" style="float:left; padding-left:1px; margin-top:-85px; font-size:15px;"><font color="#0000ff">Back to Home</font></a> -->
					<form action="submitFinalForm" method="post">
						<div class="w3-button w3-block"
						style="background-color: #76a0b7; color: #ffffff; height: 40px;">
						<p style="padding-bottom: 6px;margin-top:-3px; font-size:20px">Lab Reports</p>
					</div>
						<br>
						<div class="form-group">
							<!-- <div class="row"> -->
							<div hidden="true" class="col-xs-6">
								<label>Request ID:</label><input type="number" readonly
									class="form-control" name="request_id" value="${requestId}"
									required="required">
							</div>
							<div hidden="true" class="col-xs-6">
								<label>Member ID:</label><input type="number" readonly
									class="form-control" name="member_id"
									value="${jsonData.memberId}" required="required">
							</div>


							<div class="form-group">
								<div class="row">
									<div class="col-xs-6">
										<label>Blood Pressure:</label><input type="text"
											class="form-control" name="blood_pressure"
											placeholder="Blood Pressure" required="required" id="BP">
									</div>
									<div class="col-xs-6">
										<label>Diastolic:</label><input type="text"
											class="form-control" name="diastolic" placeholder="Diastolic"
											required="required" id="Diastolic">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-xs-6">
										<label>Systolic:</label><input type="text" id="Systolic"
											class="form-control" name="systolic" placeholder="Systolic"
											required="required">
									</div>
									<div class="col-xs-6">
										<label>Haemoglobin:</label><input type="text" id="Haemo"
											class="form-control" name="heamoglobin"
											placeholder="Haemoglobin" required="required">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-xs-4">
										<label>WBC:</label><input type="text" id="wbc" value="3300"
											class="form-control" name="wbc" placeholder="WBC"
											required="required">
									</div>
									<div class="col-xs-4">
										<label>RBC:</label><input type="text" id="rbc" value="4500"
											class="form-control" name="rbc" placeholder="RBC"
											required="required">
									</div>
									<div class="col-xs-4">
										<label>Blood Sugar Fasting:</label><input type="text" id="BSF"
											class="form-control" name="blood_sugar_fasting"
											placeholder="Blood Sugar Fasting" required="required">
									</div>
								</div>
							</div>

							<div class="form-group" align="center">
								<div class="row">

									<div class="col-xs-6">
										<!-- <button type="back" class="btn w3-button w3-block w3-theme-l1">Back</button> -->
										<form>
											<span></span> <INPUT TYPE="button" class="btn w3-button"
												style="background-color: #1cacdc; color: #ffffff"
												VALUE="Back" onClick="history.go(-1)">
										</form>
									</div>
									<div class="col-xs-6">
										<button type="submit" class="btn w3-button "
											style="background-color: #1cacdc;">
											<font color="#ffffff">Save And Review</font>
										</button>
									</div>
								</div>
							</div>
					</form>
				</div>
				<br>
				<br>
			</div>
		</div>
		<%@ include file="footer.jsp"%>
		<!--End Footer -->

	</div>
	<script>
// Accordion
function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    x.previousElementSibling.className += " w3-theme-d1";
  } else { 
    x.className = x.className.replace("w3-show", "");
    x.previousElementSibling.className = 
    x.previousElementSibling.className.replace(" w3-theme-d1", "");
  }
}

// Used to toggle the menu on smaller screens when clicking on the menu button
function openNav() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}


</script>

</body>
</html>
