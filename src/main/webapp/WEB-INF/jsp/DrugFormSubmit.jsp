
<!DOCTYPE html>
<html>
<title>Medication Reports</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>Registration Form</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	height: 100%;
	padding: 0px;
}

body {
	background-image:
		/* url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
		color : #ededed;
	font-family: 'Roboto', sans-serif;
}

body, html {
	height: 100%;
}

.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}

.form-control:focus {
	border-color: #5cb85c;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 1010px;
	height: 785px;
	margin-left: 15px;
	padding: 20px 0;
}

.signup-form h2 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form h2:before, .signup-form h2:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}

.signup-form h2:before {
	left: 0;
}

.signup-form h2:after {
	right: 0;
}

.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}

.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}

.signup-form .form-group {
	margin-bottom: 20px;
}

.signup-form input[type="checkbox"] {
	margin-top: 3px;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
	min-width: 100px;
	max-width: 200px;
	outline: none !important;
}

.signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
}

.footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}

footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}

#headerTab2 {
	background-color: #006398;
}

.contactImageStyle {
	float: right;
	position: relative;
	right: 80px;
	top: 30px;
}

.contactNumberStyle {
	position: relative;
	left: 50px;
	bottom: 35px;
}
</style>
<head>


<script>
	function backWithDelete() {
		//document.getElementById("backForm").submit();
		history.go(-1);

	}

	function getDrugNames() {
		$.ajax({
			type : "GET",
			url : "http://192.168.99.1:7002/preauth/drugnamerequest",
			dataType : "json",
			success : function(data) {
				for (var i = 0; i < data.length; i++) {
					if(data[i]!=null){
					var opt = new Option(data[i]);
					$("#dropDown").append(opt);
					}}
			},
			error : function(request, textStatus, errorThrown) {
				
			}
		})
	}

	function getMedicalDetails(value) {
		$.ajax({
			url : "http://192.168.99.1:7002/preauth/drugPARequest/" + value,
			method : "GET",
			dataType : "json",
			success : function(data) {

				$('#drugId').val(data[0].drug_id);
				
				$('#disease').val(data[0].disease);
				$('#maxDosage').val(data[0].max_dosage);
				$('#Drug_Substitute').val(data[0].drug_substitute);
				$('#Strength').val(data[0].strength);
				$('#Duration').val(data[0].duration);//clinical_info
				$('#clinical_info').val(data[0].clinical_info);
			},
			error : function(request, textStatus, errorThrown) {
			
			}
		})
	}

	$(document).ready(function() {
		getDrugNames();
	});
</script>
</head>


<body class="w3-theme-l5" id="LoginForm">
	<!-- Navbar -->
	<div class="w3-top">
		<%@ include file="header.jsp"%>
	</div>


	<!-- Navbar on small screens -->
	<div id="navDemo"
		class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
		<a href="#" class="w3-bar-item w3-button w3-padding-large">My
			Profile</a>
	</div>

	<!-- Page Container -->
	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">
		<div class="contactImageStyle">
			<img src="/images/icons/call-icon.png"/>
			<div class="contactNumberStyle">
				Contact Insurer <br>+1-987-654-3210
			</div>
		</div>

		<div class="w3-row">

			<div class="w3-col m8" style="margin-top: -8px">
				<div class="signup-form" style="width: 145%; margin-left: 15px">
					<!-- <a href="returnHome"
						style="float: left; padding-left: 1px; margin-top: -85px; font-size: 15px;"><font
						color="#0000ff">Back to Home</font></a> -->
					<form action="postDrugForm" method="post">
						<div class="w3-button w3-block"
							style="background-color: #76a0b7; color: #ffffff; height: 40px;">
							<p style="padding-bottom: 6px; margin-top: -3px; font-size: 20px">Medication
								Details</p>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6" hidden="true">
									<label>Request ID:</label><input type="number" readonly
										class="form-control" name="request_id" value="${requestId}"
										required="required">
								</div>
								<div class="col-xs-6" hidden="true">
									<label>Member ID:</label><input type="number" readonly
										class="form-control" name="member_id"
										value="${jsonData.memberId}" required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Medication ID:</label><input type="text"
										class="form-control" name="drug_id"
										placeholder="Medication ID" required="required" id="drugId">
								</div>
								<div class="col-xs-6">
									<label>Medication Name:</label> <select name="drug_name"
										id="dropDown"  class="form-control"
										onchange="getMedicalDetails(this.value)">
										<option selected="selected" id="drug_name">Select</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Diagnosis:</label><input type="text"
										class="form-control" name="disease" placeholder="diagnosis"
										required="required" id="disease">
								</div>
								<div class="col-xs-6">
									<label>Max Dosage:</label><input type="text"
										class="form-control" name="max_dosage"
										placeholder="Max Dosage" required="required" id="maxDosage">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<!-- <div class="col-xs-6">
									<label>Frequency :</label> <input type="text"
										class="form-control" placeholder="Frequency"
										name="allow_combination" required="required" id="frequency">
								</div> -->
								<div class="col-xs-6">
									<label> Substitute:</label><input type="text"
										class="form-control" name="drug_substitute"
										placeholder="Substitute" required="required"
										id="Drug_Substitute">
								</div>
								<div class="col-xs-6">
									<label>Duration:</label><input type="text" class="form-control"
										name="Duration" placeholder="Duration" required="required"
										id="Duration">
								</div>

							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Strength :</label> <input type="text"
										class="form-control" placeholder="Strength" name="Strength"
										required="required" id="Strength">
								</div>
								<div class="col-xs-6">
									<label>Clinical Information :</label> <input type="text"
										class="form-control" placeholder="clinical information"
										name="Clinical_info" id="clinical_info">
								</div>


							</div>
						</div>

						<div class="form-group" align="center">
							<div class="row">
								<div class="col-xs-6">
									<%-- <form align="center" action="deleteWithBack/${requestId}"
										id="backForm"> --%>
									<input type="button" class="btn w3-button" id="backForm"
										onClick="backWithDelete()"
										style="background-color: #1cacdc; color: #ffffff" VALUE="Back">
									<%-- <a href="deleteWithBack/${requestId}"><INPUT TYPE="button"
										class="btn w3-button"
										style="background-color: #1cacdc; color: #ffffff" VALUE="Back""></a> --%>
									<!-- </form> -->
								</div>
								<!-- </div>


							<div class="row"> -->

								<%-- <div class="col-xs-6">
									<!-- <button type="back" class="btn w3-button w3-block w3-theme-l1">Back</button> -->
									<form  action="deleteWithBack/${requestId}" id="backForm">
										<input
											type="button" class="btn w3-button  w3-theme-l1" onClick="backWithDelete()" value="Go Back">
									</form>
								</div> --%>


								<div class="col-xs-6">
									<button type="submit" class="btn w3-button "
										style="background-color: #1cacdc;">
										<font color="#ffffff">Next</font>
									</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- End Middle Column -->

		</div>
		<!-- End Grid -->
		<!-- End Page Container -->
	</div>

	<!-- Footer -->
	<!-- <div style="background-color: orange; color: blue">
			<footer class="w3-container" style="background-color: #ededed;">
				
				<p><h6><center>
					PreAuth Powered by <a href="https://www.atos-syntel.net/" target="_blank">AtoS|Syntel</a>
					</center></h6>
				</p>
			</footer>
		</div>
 --><%@ include file="footer.jsp"%>
	<script>
		// Accordion
		//Back with Delete

		function myFunction(id) {
			var x = document.getElementById(id);
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
				x.previousElementSibling.className += " w3-theme-d1";
			} else {
				x.className = x.className.replace("w3-show", "");
				x.previousElementSibling.className = x.previousElementSibling.className
						.replace(" w3-theme-d1", "");
			}
		}

		// Used to toggle the menu on smaller screens when clicking on the menu button
		function openNav() {
			var x = document.getElementById("navDemo");
			if (x.className.indexOf("w3-show") == -1) {
				x.className += " w3-show";
			} else {
				x.className = x.className.replace(" w3-show", "");
			}
		}}
	</script>
</body>
</html>
