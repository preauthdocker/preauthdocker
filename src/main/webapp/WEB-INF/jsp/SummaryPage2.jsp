<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,700"
	rel="stylesheet">
<title>Summary Page</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-blue-grey.css">
<link rel='stylesheet'
	href='https://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif
}

#LoginForm {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	padding: 0px;
}

body {
	/* background-image:
		url("https://hdwallsource.com/img/2014/9/blur-26347-27038-hd-wallpapers.jpg"); */
	color: #ededed;
	font-family: 'Roboto', sans-serif;
}

.form-control {
	height: 40px;
	box-shadow: none;
	color: #969fa4;
}

.form-control:focus {
	border-color: #5cb85c;
}

.form-control, .btn {
	border-radius: 3px;
}

.signup-form {
	width: 780px;
	margin: 0 auto;
	padding: 20px 0;
}

.signup-form h3 {
	color: #636363;
	margin: 0 0 15px;
	position: relative;
	text-align: center;
}

.signup-form h3:before, .signup-form h3:after {
	content: "";
	height: 2px;
	width: 30%;
	background: #d4d4d4;
	position: absolute;
	top: 50%;
	z-index: 2;
}

.signup-form h3:before {
	left: 0;
}

.signup-form h3:after {
	right: 0;
}

.signup-form .hint-text {
	color: #999;
	margin-bottom: 30px;
	text-align: center;
}

.signup-form form {
	color: #999;
	border-radius: 3px;
	margin-bottom: 15px;
	background: #f2f3f7;
	box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
	padding: 30px;
}

.signup-form .form-group {
	margin-bottom: 10px;
}

.signup-form input[type="checkbox"] {
	margin-top: 3px;
}

.signup-form .btn {
	font-size: 16px;
	font-weight: bold;
}

.signup-form .row div:first-child {
	padding-right: 10px;
}

.signup-form .row div:last-child {
	padding-left: 10px;
}

.signup-form a {
	color: #fff;
	text-decoration: underline;
}

.signup-form a:hover {
	text-decoration: none;
}

.signup-form form a {
	color: #5cb85c;
	text-decoration: none;
}

.signup-form form a:hover {
	text-decoration: underline;
}

.footer {
	position: fixed;
	left: 0;
	bottom: 0;
	width: 100%;
	background-color: red;
	color: grey;
}
#headerTab2{
background-color:#006398;
}
</style>

<body class="w3-theme-l5" id="LoginForm">
	<!-- Navbar -->
	<div class="w3-top" style="background-color: #ededed;">
		<div id="headerTab2" style="height:85px">
			<%-- <a
				class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2"
				href="javascript:void(0);" onclick="openNav()"><i
				class="fa fa-bars"></i></a> <a href="returnHome"
				style="color: #ffffff;"><i
				class="fa fa-home w3-margin-right"></i>PreAuth</a> <a href=""
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="Mails"><i class="fa fa-envelope"></i></a>
			<div class="w3-dropdown-hover w3-hide-small">
				<button class="w3-button w3-padding-large" title="Notifications">
					<i class="fa fa-bell"></i><span
						class="w3-badge w3-right w3-small w3-green">1</span>
				</button>
				<div class="w3-dropdown-content w3-card-4 w3-bar-block"
					style="width: 300px">
					<a href="#" class="w3-bar-item w3-button">One new claim</a>
				</div>
			</div>
			 <a
				class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white"
				title="News"><i class="fa fa-newspaper-o"></i></a> <a href="logout"
				class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white"
				title="My Account"> <img src="/images/logout123.png"
				class="w3-circle" style="height: 23px; width: 23px" alt="Logout">
			</a>
			<a href="#"
				class=" w3-right" style="color: #ffffff;"> <i
				class="fa w3-margin-right"></i>Welcome ${Provider.provider_Name}
			</a> --%>
			<%@ include file = "header.jsp" %>
		</div>
	</div>

	<!-- Page Container -->
	<div class="w3-container w3-content"
		style="max-width: 1400px; margin-top: 80px">

		<!-- The Grid -->
		<div class="w3-row">

			<!-- Left Column -->
			<div class="w3-col m2">
				<!-- Profile -->
				<div class="w3-card w3-round w3-white">
					<div class="w3-container">
						<h4 class="w3-center">My Profile</h4>
						<p class="w3-center">
							<img src="/images/avatar1.png" class="w3-circle"
								style="height: 106px; width: 106px" alt="Avatar">
						</p>
						<hr>
						<p>
							<i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme"></i>
							${Provider.provider_Name}
						</p>
						<p>
							<i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i>
							${Provider.provider_city}
						</p>
						<p>
							<i class="fa fa-user fa-fw w3-margin-right w3-text-theme"></i>
							Provider
						</p>
					</div>
				</div>
				</div> 
			

				<!-- Accordion 
				<div class="w3-card w3-round">
					<div class="w3-white">
						<button onclick="myFunction('Demo1')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-home fa-fw w3-margin-right"></i> My Home
						</button>
						<div id="Demo1" class="w3-hide w3-container">
							<p>Some text..</p>
						</div>
						<button onclick="myFunction('Demo2')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-pencil-square-o fa-fw w3-margin-right"></i>Claims
						</button>
						<div id="Demo2" class="w3-hide w3-container">
							<p>Some other text..</p>
						</div>
						<button onclick="myFunction('Demo1')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-pencil-square-o fa-fw w3-margin-right"></i>Approved
							claims
						</button>
						<div id="Demo1" class="w3-hide w3-container">
							<p>Some text..</p>
						</div>
						<button onclick="myFunction('Demo2')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-pencil-square-o fa-fw w3-margin-right"></i>Rejected
							Claims
						</button>
						<div id="Demo2" class="w3-hide w3-container">
							<p>Some other text..</p>
						</div>
						<button onclick="myFunction('Demo1')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-plus-square fa-fw w3-margin-right"></i>Linked
							Hospitals
						</button>
						<div id="Demo1" class="w3-hide w3-container">
							<p>Some text..</p>
						</div>
						<button onclick="myFunction('Demo2')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-user-plus fa-fw w3-margin-right"></i>linked
							doctors
						</button>
						<div id="Demo2" class="w3-hide w3-container">
							<p>Some other text..</p>
						</div>
						<button onclick="myFunction('Demo1')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-user-circle fa-fw w3-margin-right"></i>My Profile
						</button>
						<div id="Demo1" class="w3-hide w3-container">
							<p>Some text..</p>
						</div>
						<button onclick="myFunction('Demo2')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="fa fa-vcard-o fa-fw w3-margin-right"></i> Summary
						</button>
						<div id="Demo2" class="w3-hide w3-container">
							<p>Some other text..</p>
						</div>
						<!-- <button onclick="myFunction('Demo3')"
							class="w3-button w3-block w3-theme-l1 w3-left-align">
							<i class="	fa fa-power-off fa-fw w3-margin-right"></i> LogOut
						</button> 
					</div>
				</div>  -->
			
			
			<!-- End Left Column -->

			<!-- Middle Column -->
			<div class="w3-col m8">
				
				<div class="signup-form">
<%-- 					<form action="summaryPostPaRequest2" method="get">
						<h3>Personal Details</h3>

						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Member ID :</label><input type="number"
										class="form-control" name="member_id"
										value="${PersonalDetails.member_id}" required="required">
								</div>
								<div class="col-xs-6">
									<label>Request ID :</label><input type="number"
										class="form-control" name="request_id" value="${requestId}"
										required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>First Name:</label><input type="text"
										value="${PersonalDetails.first_name}" class="form-control"
										name="first_name" placeholder="First Name" required="required">
								</div>
								<div class="col-xs-6">
									<label>Last Name:</label><input type="text"
										class="form-control" name="last_name"
										value="${PersonalDetails.last_name}" placeholder="Last Name"
										required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Sex:</label><input type="text"
										value="${PersonalDetails.sex}" class="form-control" name="sex"
										placeholder="sex" required="required">
								</div>
								<div class="col-xs-4">
									<label>Age:</label><input type="number"
										value="${PersonalDetails.age}" class="form-control" name="age"
										placeholder="age" required="required">
								</div>
								<div class="col-xs-4">
									<label>DOB:</label><input type="date"
										value="${PersonalDetails.dob}" class="form-control" name="dob"
										placeholder="dob" required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Insured Status:</label><input type="text"
										class="form-control" value="${PersonalDetails.insured}"
										name="insured" placeholder="insured" required="required">
								</div>
								<div class="col-xs-6">
									<label>Payer ID:</label><input type="text" class="form-control"
										value="${PersonalDetails.payer_id}" name="payer_id"
										placeholder="payer_id" required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Policy ID:</label><input type="text"
										class="form-control" value="${PersonalDetails.policy_id}"
										name="policy_id" placeholder="policy_id" required="required">
								</div>
								<div class="col-xs-6">
									<label>Latest Treatment Date:</label><input type="text"
										class="form-control"
										value="${PersonalDetails.latest_treatment_date}"
										name="latest_treatment_date"
										placeholder="latest_treatment_date" required="required">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Vital Status:</label><input type="text"
										class="form-control" value="${PersonalDetails.vital_status}"
										name="vital_status" placeholder="vital_status"
										required="required">
								</div>
								<div class="col-xs-6">
									<label>Source of Vital Status:</label><input type="text"
										value="${PersonalDetails.last_reporting_source}"
										class="form-control" name="last_reporting_source"
										placeholder="last_reporting_source" required="required">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Provider ID:</label><input type="text"
										class="form-control" value="${PersonalDetails.provider_id}"
										name="provider_id" placeholder="provider_id"
										required="required">
								</div>
								<div class="col-xs-6">
									<label>Request Status:</label><input type="text"
										class="form-control" value="${PersonalDetails.current_status}"
										name="current_status" placeholder="request_status"
										required="required">
								</div> 
							</div>
						</div>
						<!-- <div class="form-group">
							<button type="submit" class="btn btn-success btn-lg  ">Update</button>
						</div> -->
					</form> --%>
					
					<form action="summaryPostPaRequest2" method="get">
						<h3>Personal Details</h3>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Member ID : </label><label>
										${PersonalDetails.member_id}</label>
								</div>
								<div class="col-xs-4">
									<label>Request ID : </label><label> ${requestId}</label>
								</div>
								<div class="col-xs-4">
									<label>Name : </label><label>
										${PersonalDetails.first_name} ${PersonalDetails.last_name}</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-4">
									<label>Gender:</label><label>${PersonalDetails.sex}</label>
								</div>
								<div class="col-xs-4">
									<label>Age:</label><label>${PersonalDetails.age}</label>
								</div>
								<div class="col-xs-4">
									<label>DOB:</label><label>${PersonalDetails.dob}</label>
								</div>
							</div>
						</div>
						</form>
					<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

					<form action="summarydrugPaRequest" method="post">
						<h3>Medication Details</h3>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<!-- <label>Request ID:</label> --><input type="number" hidden
										class="form-control" name="request_id"
										value="${DrugDetails.request_id}" required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Dignosis:</label><input type="text" class="form-control"
										value="${DrugDetails.disease}" name="Dignosis"
										placeholder="Dignosis" required="required" id="disease">
								</div>
								<div class="col-xs-6">
									<label>Medication ID:</label><input type="text" class="form-control"
										value="${DrugDetails.drug_id}" name="drug_id"
										placeholder="Medication ID" required="required" id="drugId">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Medication Name:</label><input type="text"
										class="form-control" name="drug_name" placeholder="Medication Name"
										value="${DrugDetails.drug_name}" required="required"
										id="drugName">
								</div>
								<div class="col-xs-6">
									<label>Medication Substitute:</label><input type="text"
										value="${DrugDetails.drug_substitute}" class="form-control"
										name="drug_substitute" placeholder="Medication Substitute"
										required="required">
								</div>
							</div>
						</div>



						<div class="form-group">
							<div class="row">

								<div class="col-xs-6">
									<label>Max Dosage:</label><input type="text"
										value="${DrugDetails.max_dosage}" class="form-control"
										name="max_dosage" placeholder="Max Dosage" required="required"
										id="maxDosage">
								</div>
								<div class="col-xs-6">
									<label>Allow Combination :</label> <input type="text"
										value="${DrugDetails.allow_combination}" class="form-control"
										placeholder="Allowed Combination" name="allow_combination"
										required="required">
								</div>
							
							</div>
						</div>
						<div class="form-group"></div>
						<div class="form-group" align="center">
							<button type="submit" class="btn btn-success">Update</button>
							<button type="submit" class="btn btn-success">Reset</button>
							<button type="submit" class="btn btn-success">Cancel</button>
						</div>
					</form>

					<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->

					<!-- <form action="summaryLabForm" method="post">
						<h3>LabReports</h3>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<!-- <label>Request ID:</label> 
									<input type="number" hidden
										class="form-control" name="request_id"
										value="${LabDetails.request_id}" required="required">
								</div>
								<%--	<div class="col-xs-6">
									<label>Member ID:</label><input type="number"
										class="form-control" name="member_id"
										value="${jsonData.memberId}" required="required">--%>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Blood Pressure:</label><input type="text"
										value="${LabDetails.blood_pressure}" class="form-control"
										name="blood_pressure" placeholder="Blood Pressure"
										required="required" id="BP">
								</div>
								<div class="col-xs-6">
									<label>Diastolic:</label><input type="text"
										value="${LabDetails.diastolic}" class="form-control"
										name="diastolic" placeholder="Diastolic" required="required"
										id="Diastolic">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Systolic:</label><input type="text" id="Systolic"
										value="${LabDetails.systolic}" class="form-control"
										name="systolic" placeholder="Systolic" required="required">
								</div>
								<div class="col-xs-6">
									<label>Haemoglobin:</label><input type="text" id="Haemo"
										value="${LabDetails.heamoglobin}" class="form-control"
										name="heamoglobin" placeholder="Haemoglobin"
										required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>WBC:</label><input type="text" id="wbc"
										value="${LabDetails.wbc}" class="form-control" name="wbc"
										placeholder="WBC" required="required">
								</div>
								<div class="col-xs-6">
									<label>RBC:</label><input type="text" id="rbc"
										value="${LabDetails.rbc}" class="form-control" name="rbc"
										placeholder="RBC" required="required">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>Blood Sugar Fasting:</label><input type="text" id="BSF"
										value="${LabDetails.blood_sugar_fasting}" class="form-control"
										name="blood_sugar_fasting" placeholder="Blood Sugar Fasting"
										required="required">
								</div>
								<div class="col-xs-6">
									<label>HIV Status:</label><input type="text"
										value="${LabDetails.hiv_status}" class="form-control" id="HIV"
										name="hiv_status" placeholder="Hiv Status" required="required">
								</div>
							</div>
						</div>
						<div class="form-group" align="center">
							<a class="trigger_popup_fricc">
								<button type="submit" class="btn btn-success">Update</button>
								<button type="submit" class="btn btn-success">Reset</button>
								<button type="submit" class="btn btn-success">Cancel</button>
							</a>
						</div>
					</form> -->
					<form action="ViewPaRequest">
						<div class="form-group" align="center">
							<a class="trigger_popup_fricc">
								<button type="submit" class="btn btn-success" align=center>Done</button>
							</a>
						</div>
					</form>
				</div>
			</div>  
			
		
			
			<!--m8 Closes Here>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->

			<!--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@-->
			<!-- End Middle Column -->

			<!-- Start Right Column -->
			<div class="w3-col m2">
				<!-- <div class="w3-card w3-round w3-white w3-center">
					<div class="w3-container">
						<br>
						<p>Powered By</p>
						<img src="/images/atos-syntel1.png"
							style="height: 50px; width: 150px"> <br> <br>
					</div>
				</div> -->
				<br>
				<!-- <div class="w3-card w3-round w3-white w3-padding-16 w3-center">
					<p>EIS Practice Demo</p>
				</div> -->
				<br>
				<!-- <div class="w3-card w3-round w3-white w3-padding-32 w3-center">
					<h4>Contact Insurer</h4>
					<p>
						<i class="fa fa-phone" style="font-size: 36px"></i>
					</p>
					<h4>+1-987-654-321</h4>
				</div> -->
			</div>
			<!-- End Right Column -->
		</div>
		
		<!-- End Grid -->

		<!-- End Page Container -->

		<!-- Footer -->
		<!-- <div class="footer" style="background-color: orange; color: blue">
			<footer class="w3-container" style="background-color: #ededed;">
				
				<p><h6><center>
					PreAuth Powered by <a href="https://www.atos-syntel.net/" target="_blank">AtoS|Syntel
					</a></center></h6>
				</p>
			</footer>
			</div> -->
			<%@ include file = "footer.jsp" %>
			<!-- End Footer -->
		
	</div>
	<script>
// Accordion
function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
    x.previousElementSibling.className += " w3-theme-d1";
  } else { 
    x.className = x.className.replace("w3-show", "");
    x.previousElementSibling.className = 
    x.previousElementSibling.className.replace(" w3-theme-d1", "");
  }
}

// Used to toggle the menu on smaller screens when clicking on the menu button
function openNav() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

.login-form .form-control {
	  background: #f7f7f7 none repeat scroll 0 0;
	  border: 1px solid #d4d4d4;
	  border-radius: 4px;
	  font-size: 14px;
	  height: 50px;
	  line-height: 50px;
	}
</script>
</body>
</html>
