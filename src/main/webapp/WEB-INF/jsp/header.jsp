<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<title>Provider Portal</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">



<style>
html, body, h1, h2, h3, h4, h5 {
	/* font-family: "Open Sans", sans-serif */
	font-family: "Roboto", sans-serif;
}

#searchError {
	color: red;
}

#WelcomeProvider {
	align: 10px;
}

#font {
	font-style: Roboto;
}

.top-right {
	position: absolute;
	top: 22px;
	left: 1210px;
}

#headerTab2 {
	background-color: #006398;
}

.titleStyle {
	color: #ffffff;
	font-size: 30px;
	padding-left: 100px;
	padding-top: 20px;
	font-style: bold;
	font-family: "Roboto", sans-serif;
}

.userStyle {
	position:absolute;
	color: #ffffff;
	font-size: 15px;
	margin-top: -60px;
	margin-right: 130px;
	text-align: right;
	
}

.logoutIconStyle {
	margin-left: 70px;
	margin-top: -90px;
	width: 25%;
}

.bellIconStyle {
	margin-top: -75px;
	margin-left: 117px;
	color: #ffffff;
}

.rightComponent{

	position:absolute;
	right:50px;
}
.homeIconStyle {

	position: absolute;
	top: 0;
	left: 0;
	
}

.homeIconStyle {
	z-index: 10;
}

.homeIconImageStyle{
	width: 95%;
	height: 95%;
}
</style>
<script >
$(document)
.ready(
		function() {
			if(document.title==="ProviderPortalHomePage"){
				
				document.getElementById("home").style.visibility = "hidden";
				document.getElementById("preauth1").style.paddingLeft = "30px";
			}
	
		});
</script>
<body>
         <a id="home" href="returnHome"><div class="homeIconStyle">
		<img src="/images/icons/Home.png" class="homeIconImageStyle">
	</div></a>
		<div id="headerTab2" style="height: 85px;">
			<p class="titleStyle" id="preauth1">PreAuth</p>
			<div class="rightComponent">
			<div style="float: right; margin-top:3px" >
				<p class="userStyle">
					Welcome<br>${Provider.provider_Name}
				</p>
				<a href="logout"><img class="logoutIconStyle" src="/images/icons/Logout.png" /></a>
				<div class="bellIconStyle">
					<i class="bell fa fa-bell-o w3-xxlarge"></i> <span
						class="w3-badge w3-right w3-small w3-orange top-right">1</span>
				</div>
			</div>
			</div>
		</div>
</body>
</html>